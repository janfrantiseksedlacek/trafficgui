package experimental3D;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;


/**
 *
 */
public class Carmodel extends JPanel {


    /**
     *
     */
    private ArrayList<Shape3D> shape3DS = new ArrayList<>();

    /**
     *
     */
    private Cube3D a = new Cube3D(0, 0, 0, 50, 450, 100, 1);

    /**
     *
     */
    private Cube3D b = new Cube3D(0, 0, 0, -50, 450, 100, 1);

    /**
     *
     */
    private Cube3D c = new Cube3D(0, 0, 0, 10, 50, 25, 1);


    /**
     *
     */
    public Carmodel() {

        shape3DS.add(a);
        shape3DS.add(b);
        shape3DS.add(c);
        c.setPosition(new Point3D(50, 25, 25));


    }


    /**
     * @param g
     */
    @Override
    protected void paintComponent(Graphics g) {

        super.paintComponent(g);

        Graphics2D g3 = (Graphics2D) g;


        g3.translate(0, getHeight());


        g3.scale(1, -1);


        super.paintComponent(g);


    }

}
