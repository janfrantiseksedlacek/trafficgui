package experimental3D;

import java.awt.*;

/**
 *
 */
public class Cube3D  extends Shape3D{

    /**
     * @return
     */
    public Rectangle3D getR1() {
        return r1;
    }

    /**
     * @param r1
     */
    public void setR1(Rectangle3D r1) {
        this.r1 = r1;
    }

    /**
     * @return
     */
    public Rectangle3D getR2() {
        return r2;
    }

    /**
     * @param r2
     */
    public void setR2(Rectangle3D r2) {
        this.r2 = r2;
    }

    /**
     *
     */
    private Rectangle3D r1;

    /**
     *
     */
    private Rectangle3D r2;

    /**
     * @param x
     * @param y
     * @param z
     * @param width
     * @param height
     * @param depht
     * @param d
     */
    public Cube3D(double x,double y,double z,double width,double height,double depht,double d)
    {
        r1=new Rectangle3D(x,y,z,width,height,d);

        r2=new Rectangle3D(x,y,depht,width,height,d);



        r1.setPosition(new Point3D(x, y, z));
        r2.setPosition(new Point3D(x, y, z));

    }

    /**
     * @param g
     */
    @Override
    public void drawTo2D(Graphics2D g) {

        g.setColor(Color.RED);
        r1.drawTo2D(g);
        r2.drawTo2D(g);
        g.setColor(Color.BLUE);
        new Line3D(r1.getL1().getP1(),r2.getL1().getP1(),r1.getL1().getD()).drawTo2D(g);
        new Line3D(r1.getL2().getP2(),r2.getL2().getP2(),r1.getL1().getD()).drawTo2D(g);
        new Line3D(r1.getL3().getP2(),r2.getL3().getP2(),r1.getL1().getD()).drawTo2D(g);
        new Line3D(r1.getL4().getP1(),r2.getL4().getP1(),r1.getL1().getD()).drawTo2D(g);

    }


    /**
     * @param g
     */
    @Deprecated
    public void drawTo2Dfill(Graphics2D g) {

        g.setColor(Color.BLACK);
        r1.drawTo2Dfill(g);
        r2.drawTo2Dfill(g);

        Line3D l1=  new Line3D(r1.getL1().getP1(),r2.getL1().getP1(),r1.getL1().getD());
        Line3D l2=   new Line3D(r1.getL2().getP2(),r2.getL2().getP2(),r1.getL1().getD());
        Line3D l3=  new Line3D(r1.getL3().getP2(),r2.getL3().getP2(),r1.getL1().getD());
        Line3D l4=  new Line3D(r1.getL4().getP1(),r2.getL4().getP1(),r1.getL1().getD());

        g.setColor(Color.RED);
        int[] x=new int[]{(int)l1.get2Dline(g).getX1(),(int)l1.get2Dline(g).getX2(),(int)l2.get2Dline(g).getX2(),(int)l2.get2Dline(g).getX1()};
        int[] y=new int[]{(int)l1.get2Dline(g).getY1(),(int)l1.get2Dline(g).getY2(),(int)l2.get2Dline(g).getY2(),(int)l2.get2Dline(g).getY1()};

        Polygon tmp= new Polygon(x, y, 4);


        g.fill(tmp);

         x=new int[]{(int)l3.get2Dline(g).getX1(),(int)l3.get2Dline(g).getX2(),(int)l4.get2Dline(g).getX2(),(int)l4.get2Dline(g).getX1()};
         y=new int[]{(int)l3.get2Dline(g).getY1(),(int)l3.get2Dline(g).getY2(),(int)l4.get2Dline(g).getY2(),(int)l4.get2Dline(g).getY1()};

         tmp= new Polygon(x, y, 4);

        g.setColor(Color.BLUE);
        g.fill(tmp);

         x=new int[]{(int)l2.get2Dline(g).getX1(),(int)l2.get2Dline(g).getX2(),(int)l4.get2Dline(g).getX2(),(int)l4.get2Dline(g).getX1()};
         y=new int[]{(int)l2.get2Dline(g).getY1(),(int)l2.get2Dline(g).getY2(),(int)l4.get2Dline(g).getY2(),(int)l3.get2Dline(g).getY1()};

         tmp= new Polygon(x, y, 4);

        g.setColor(Color.GREEN);
        g.fill(tmp);

         x=new int[]{(int)l4.get2Dline(g).getX1(),(int)l4.get2Dline(g).getX2(),(int)l3.get2Dline(g).getX2(),(int)l3.get2Dline(g).getX1()};
         y=new int[]{(int)l4.get2Dline(g).getY1(),(int)l4.get2Dline(g).getY2(),(int)l3.get2Dline(g).getY2(),(int)l3.get2Dline(g).getY1()};

        tmp= new Polygon(x, y, 4);

        g.setColor(Color.YELLOW);
        g.fill(tmp);

    }

    /**
     * @param alfa
     */
    @Override
    public void transformAroundZ(double alfa) {
        r1.transformAroundZ(alfa);
        r2.transformAroundZ(alfa);
    }

    /**
     * @param alfa
     */
    @Override
    public void transformAroundX(double alfa) {
        r1.transformAroundX(alfa);
        r2.transformAroundX(alfa);

    }

    /**
     * @param alfa
     */
    @Override
    public void transformAroundY(double alfa) {
        r1.transformAroundY(alfa);
        r2.transformAroundY(alfa);
    }

    /**
     * @param p
     */
    @Override
    public void setPosition(Point3D p) {
        r1.setPosition(p);
        r2.setPosition(p);
    }
}
