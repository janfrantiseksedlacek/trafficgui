package experimental3D;

import java.awt.geom.Point2D;

/**
 *
 */
public class Point3D implements IRotation3D {

    /**
     *
     */
    private double x;

    /**
     *
     */
    private double y;

    /**
     *
     */
    private double z;

    /**
     *
     */
    private double positionX = 0;

    /**
     *
     */
    private double positionY = 0;

    /**
     *
     */
    private double positionZ = 0;

    /**
     * @return
     */
    public double getPositionX() {
        return positionX;
    }

    /**
     * @param positionX
     */
    public void setPositionX(double positionX) {
        this.positionX = positionX;
    }

    /**
     * @return
     */
    public double getPositionY() {
        return positionY;
    }

    /**
     * @param positionY
     */
    public void setPositionY(double positionY) {
        this.positionY = positionY;
    }

    /**
     * @return
     */
    public double getPositionZ() {
        return positionZ;
    }

    /**
     * @param positionZ
     */
    public void setPositionZ(double positionZ) {
        this.positionZ = positionZ;
    }


    /**
     * @param x
     * @param y
     * @param z
     */
    public Point3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;


    }

    /**
     * @param p
     */
    public  void setPosition(Point3D p)
    {
        positionY =p.y;
        positionX =p.x;
        positionZ =p.z;


    }

    /**
     * @return
     */
    public Point2D toPoint2D()
    {

        return new Point2D.Double(getX(),getY());
    }


    /**
     * @return
     */
    public double getX() {
        return x + positionX;
    }

    /**
     * @param x
     */
    public void setX(double x) {
        this.x = x;
    }

    /**
     * @return
     */
    public double getY() {
        return y+ positionY;
    }

    /**
     * @param y
     */
    public void setY(double y) {
        this.y = y;
    }

    /**
     * @return
     */
    public double getZ() {
        return z+ positionZ;
    }

    /**
     * @param z
     */
    public void setZ(double z) {
        this.z = z;
    }

    /**
     * @param alfa
     */
    @Override
    public void transformAroundZ(double alfa)
    {


if(alfa>0)
        for (double i=0;i<alfa;i+=0.001)
        {
            double sin= Math.sin(Math.toRadians(0.001));
            double cos= Math.cos(Math.toRadians(0.001));
            x = x *cos+-y*sin;
            y= x *sin+y*cos;
        }
else
    for (double i=0;i>alfa;i+=-0.001)
    {
        double sin= Math.sin(Math.toRadians(-0.001));
        double cos= Math.cos(Math.toRadians(-0.001));
        x = x *cos-y*sin;
        y= x *sin+y*cos;
    }


    }

    /**
     * @param alfa
     */
    @Override
    public void transformAroundX(double alfa)
    {
        if(alfa>0)
            for (double i=0;i<alfa;i+=0.001)
            {
                double sin= Math.sin(Math.toRadians(0.001));
                double cos= Math.cos(Math.toRadians(0.001));
                y=y*cos-z*sin;
                z=y*sin+z*cos;
            }
        else
            for (double i=0;i>alfa;i+=-0.001)
            {
                double sin= Math.sin(Math.toRadians(-0.001));
                double cos= Math.cos(Math.toRadians(-0.001));
                y=y*cos-z*sin;
                z=y*sin+z*cos;
            }


    }

    /**
     * @param alfa
     */
    @Override
    public void transformAroundY(double alfa)
    {

        if(alfa>0)
            for (double i=0;i<alfa;i+=0.001)
            {
                double sin= Math.sin(Math.toRadians(0.001));
                double cos= Math.cos(Math.toRadians(0.001));
                x = x *cos+z*sin;
                z=-x *sin+z*cos;
            }
        else
            for (double i=0;i>alfa;i+=-0.001)
            {
                double sin= Math.sin(Math.toRadians(-0.001));
                double cos= Math.cos(Math.toRadians(-0.001));
                x = x *cos+z*sin;
                z=-x *sin+z*cos;
            }


    }

    /**
     * @return
     */
    public boolean hasNegative(){

        return !(x >= 0) || !(y >= 0) || !(z >= 0);

    }



}
