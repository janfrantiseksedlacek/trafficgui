package experimental3D;

import java.awt.*;
import java.awt.geom.Line2D;

/**
 *
 */
public class Line3D extends Shape3D {


    /**
     *
     */
    private Point3D p1;

    /**
     *
     */
    private Point3D p2;

    /**
     *
     */
    @SuppressWarnings("There no implementation od this value")
    private double d;

    /**
     * @param p1
     * @param p2
     * @param d
     */
    public Line3D(Point3D p1, Point3D p2,double d) {
        this.p1 = p1;
        this.p2 = p2;
        this.d = d;


    }

    /**
     * @param p
     */
    public void setPosition(Point3D p)
    {

        p1.setPosition(p);
        p2.setPosition(p);

    }

    /**
     * @return
     */
    public Point3D getP1() {
        return p1;
    }

    /**
     * @param p1
     */
    public void setP1(Point3D p1) {
        this.p1 = p1;
    }

    /**
     * @return
     */
    public Point3D getP2() {
        return p2;
    }

    /**
     * @param p2
     */
    public void setP2(Point3D p2) {
        this.p2 = p2;
    }


    /**
     * @param g
     */
    @Override
    public void drawTo2D(Graphics2D g)
    {


        double add1=getP1().getZ()*(3/4);
        double add2=getP2().getZ()*(3/4);



        if(add1==Double.NaN)
            add1=0;

        if(add2==Double.NaN)
            add2=0;

        if(add2==Double.NEGATIVE_INFINITY)
        add2=0;

        if(add1==Double.NEGATIVE_INFINITY)
            add1=0;

        if(add2==Double.POSITIVE_INFINITY)
            add2=0;

        if(add1==Double.POSITIVE_INFINITY)
            add1=0;


        g.drawLine(
                (int)((getP1().getX()+((add1)))),
                (int)((getP1().getY()+((add1)))),
                (int)((getP2().getX()+((add2)))),
                (int)((getP2().getY()+((add2)))));


    }


    /**
     * @param g
     * @return
     */
    public Line2D.Double get2Dline(Graphics2D g)
    {


        double add1=getP1().getZ()*(3/4);
        double add2=getP2().getZ()*(3/4);



        if(add1==Double.NaN)
            add1=0;

        if(add2==Double.NaN)
            add2=0;

        if(add2==Double.NEGATIVE_INFINITY)
            add2=0;

        if(add1==Double.NEGATIVE_INFINITY)
            add1=0;

        if(add2==Double.POSITIVE_INFINITY)
            add2=0;

        if(add1==Double.POSITIVE_INFINITY)
            add1=0;


       return new Line2D.Double(
                (int)((getP1().getX()+((add1)))),
                (int)((getP1().getY()+((add1)))),
                (int)((getP2().getX()+((add2)))),
                (int)((getP2().getY()+((add2)))));


    }


    /**
     * @param alfa
     */
    @Override
    public void transformAroundZ(double alfa)
    {

        p1.transformAroundZ(alfa);
        p2.transformAroundZ(alfa);

    }

    /**
     * @param alfa
     */
    @Override
    public void transformAroundX(double alfa)
    {

        p1.transformAroundX(alfa);
        p2.transformAroundX(alfa);

    }

    /**
     * @param alfa
     */
    @Override
    public void transformAroundY(double alfa)
    {

        p1.transformAroundY(alfa);
        p2.transformAroundY(alfa);

    }

    /**
     * @return
     */
    public double getD() {
        return d;
    }
}
