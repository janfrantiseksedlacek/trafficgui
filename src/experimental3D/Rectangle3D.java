package experimental3D;

import java.awt.*;

/**
 *
 */
public class Rectangle3D extends Shape3D {

    /**
     *
     */
    private Line3D l1;

    /**
     * @param l1
     */
    public void setL1(Line3D l1) {
        this.l1 = l1;
    }

    /**
     * @param l2
     */
    public void setL2(Line3D l2) {
        this.l2 = l2;
    }

    /**
     * @param l3
     */
    public void setL3(Line3D l3) {
        this.l3 = l3;
    }

    /**
     * @param l4
     */
    public void setL4(Line3D l4) {
        this.l4 = l4;
    }

    /**
     *
     */
    private Line3D l2;
    /**
     *
     */
    private Line3D l3;
    /**
     *
     */
    private Line3D l4;


    /**
     * @param x
     * @param y
     * @param z
     * @param width
     * @param height
     * @param d
     */
    public Rectangle3D(double x,double y,double z,double width,double height,double d)
    {


        this.l1=new Line3D(new Point3D(x, y, z), new Point3D(x, y+height, z),d);

        this.l2=new Line3D(new Point3D(x, y, z), new Point3D(x+width, y, z),d);

        this.l3=new Line3D(new Point3D(x+width, y+height, z), new Point3D(x, y+height, z),d);

        this.l4=new Line3D(new Point3D(x+width, y+height, z), new Point3D(x+width, y, z),d);



    }


    /**
     * @param p
     */
    public void setPosition(Point3D p)
    {

        l1.setPosition(p);
        l2.setPosition(p);
        l3.setPosition(p);
        l4.setPosition(p);
    }


    /**
     * @param g
     */
    @Override
    public void drawTo2D(Graphics2D g) {
        g.setColor(Color.GREEN);
        l1.drawTo2D(g);
        g.setColor(Color.BLACK);
        l2.drawTo2D(g);
        g.setColor(Color.BLUE);
        l3.drawTo2D(g);
        g.setColor(Color.RED);
        l4.drawTo2D(g);

    }


    /**
     * @param g
     */
    public void drawTo2Dfill(Graphics2D g) {

       int[] x=new int[]{(int)l1.get2Dline(g).getX1(),(int)l1.get2Dline(g).getX2(),(int)l4.get2Dline(g).getX1(),(int)l4.get2Dline(g).getX2()};
        int[] y = new int[]{(int) l1.get2Dline(g).getY1(), (int) l1.get2Dline(g).getY2(), (int) l4.get2Dline(g).getY1(), (int) l4.get2Dline(g).getY2()};


        g.fillPolygon(x, y, 4);



    }


    /**
     * @param alfa
     */
    @Override
    public void transformAroundZ(double alfa)
    {
        l1.transformAroundZ(alfa);
        l2.transformAroundZ(alfa);
        l3.transformAroundZ(alfa);
        l4.transformAroundZ(alfa);


    }

    /**
     * @param alfa
     */
    @Override
    public void transformAroundX(double alfa)
    {
        l1.transformAroundX(alfa);
        l2.transformAroundX(alfa);
        l3.transformAroundX(alfa);
        l4.transformAroundX(alfa);
    }

    /**
     * @param alfa
     */
    @Override
    public void transformAroundY(double alfa)
    {
        l1.transformAroundY(alfa);
        l2.transformAroundY(alfa);
        l3.transformAroundY(alfa);
        l4.transformAroundY(alfa);
    }

    /**
     * @return
     */
    public Line3D getL1() {
        return l1;
    }

    /**
     * @return
     */
    public Line3D getL2() {
        return l2;
    }

    /**
     * @return
     */
    public Line3D getL3() {
        return l3;
    }

    /**
     * @return
     */
    public Line3D getL4() {
        return l4;
    }
}
