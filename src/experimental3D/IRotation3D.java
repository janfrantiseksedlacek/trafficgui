package experimental3D;

public interface IRotation3D {

    /**
     * @param alfa
     */
    void transformAroundZ(double alfa);

    /**
     * @param alfa
     */
    void transformAroundX(double alfa);

    /**
     * @param alfa
     */
    void transformAroundY(double alfa);
}
