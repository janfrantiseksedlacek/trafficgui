package experimental3D;

import java.awt.*;

/**
 *
 */
public interface IDraw3D {
    /**
     * @param g
     */
    void drawTo2D(Graphics2D g);
}
