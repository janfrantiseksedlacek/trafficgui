package seminarni_prace;

import TrafficSim.Car;

import javax.swing.*;
import javax.swing.event.AncestorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;


/**
 *
 */
public class GUI implements ActionListener, IClickedOnObject {

    /**
     *
     */
    private JPanel MainPanel;
    /**
     *
     */
    private JPanel jpanelList;
    /**
     *
     */
    private JPanel jpanelTab;
    /**
     *
     */
    private JPanel jpanelDraw;
    /**
     *
     */
    private JList jlistRoads;
    /**
     *
     */
    private JList<Car> jlistCars;
    /**
     *
     */
    private JList jlistCross;
    /**
     *
     */
    private JButton jbuttoncrossesButton;
    /**
     *
     */
    private JButton jbuttonCars;
    /**
     *
     */
    private JButton jbuttonRoads;
    /**
     *
     */
    private JComboBox<String> comboBox1;
    /**
     *
     */
    private JSlider slider1;
    /**
     *
     */
    private JLabel jlabelSlider;
    /**
     *
     */
    private JScrollPane jscrollRoads;
    /**
     *
     */
    private JScrollPane jscrollCross;
    /**
     *
     */
    private JScrollPane jscrollCars;
    /**
     *
     */
    private JSlider slider2;
    /**
     *
     */
    private JLabel jlabelSlidej2;
    /**
     *
     */
    private JRadioButton trafficVisibilityRadioButton;
    /**
     *
     */
    private JTabbedPane tabbedPane1;
    /**
     *
     */
    private JPanel Graph1;
    /**
     *
     */
    private JButton ResetAllGraph;
    /**
     *
     */
    private JButton xTickminus;
    /**
     *
     */
    private JButton Xtickplus;
    /**
     *
     */
    private JButton Ytickminus;
    /**
     *
     */
    private JButton Ytickplus;
    /**
     *
     */
    private JCheckBox movingAvgCheckBox;
    /**
     *
     */
    private JSpinner spinner1;
    /**
     *
     */
    private JButton ExportgrafData;
    /**
     *
     */
    private JButton ExportgrafSvg;
    /**
     *
     */
    private JTextPane info;
    /**
     *
     */
    private JPanel model3D;
    /**
     *
     */
    private JPanel back;
    /**
     *
     */
    private JSplitPane split;
    /**
     *
     */
    private JPanel relaxBrick;
    /**
     *
     */
    private JRadioButton Ncars;
    /**
     *
     */
    private JRadioButton SpeedB;
    /**
     *
     */
    private JButton exportAsSvgButton;
    /**
     *
     */
    private DrawingPanel drawingPanel;
    /**
     *
     */
    private DrawingPanel3D drawingPanel3D;
    /**
     *
     */
    private Graph graph;
    /**
     *
     */
    private Timer timer;
    /**
     *
     */
    private double delay = 1;
    /**
     *
     */
    private double next = 0;
    /**
     *
     */
    private double tmpN = 0;
    /**
     *
     */
    private DecimalFormat dec = new DecimalFormat("#0000");

    /**
     *
     */
    public GUI() {


        $$$setupUI$$$();
        jlabelSlidej2.setText("NextStep: " + dec.format(next) + "ms");

        jlabelSlider.setText("Delay: " + dec.format(delay) + "ms");

        timer = new Timer((int) delay, this);

        timer.start();

        control();
    }

    static String arg = "0";


    /**
     * @param args
     */
    public static void main(String[] args) {

        if (args.length > 0) {
            arg = args[0];
        }
        JFrame frame = new JFrame("GUI");
        GUI a = new GUI();
        frame.setContentPane(a.MainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

    }

    /**
     *
     */
    public void control() {


        jpanelDraw.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
                super.mouseMoved(e);
                try {
                    if (drawingPanel.getMousePosition() != null)
                        info.setText(drawingPanel.getMessege(drawingPanel.getMousePosition()));

                } catch (NoninvertibleTransformException e1) {
                    e1.printStackTrace();
                }
            }
        });

        model3D.addMouseMotionListener(new MouseMotionAdapter() {

            int previousY;
            int previousX;

            @Override
            public void mouseMoved(MouseEvent e) {
                previousY = e.getY();
                previousX = e.getX();
            }

            @Override
            public void mouseDragged(MouseEvent e) {

                int y = e.getY();
                int x = e.getX();
                if (y < previousY) {
                    for (Shape3D tmp : drawingPanel3D.getTrafficState().getShapes3D())
                        tmp.transformAroundX(2);

                } else if (y > previousY) {
                    for (Shape3D tmp : drawingPanel3D.getTrafficState().getShapes3D())
                        tmp.transformAroundX(-2);
                } else if (x < previousX) {
                    for (Shape3D tmp : drawingPanel3D.getTrafficState().getShapes3D())
                        tmp.transformAroundY(2);

                } else if (x > previousX) {
                    for (Shape3D tmp : drawingPanel3D.getTrafficState().getShapes3D())
                        tmp.transformAroundY(-2);
                }
                previousY = y;
                previousX = x;
                model3D.repaint();

            }
        });

        jpanelDraw.addMouseMotionListener(new MouseMotionAdapter() {

            int previousY;
            int previousX;

            @Override
            public void mouseMoved(MouseEvent e) {
                previousY = e.getY();
                previousX = e.getX();
                drawingPanel.repaint();
                drawingPanel.getParent().repaint();

            }


            @Override
            public void mouseDragged(MouseEvent e) {
                super.mouseDragged(e);

                int y = e.getY();
                int x = e.getX();


                drawingPanel.setTranX((x - previousX) + drawingPanel.getTranX());
                drawingPanel.setTranY((y - previousY) + drawingPanel.getTranY());
                previousY = y;
                previousX = x;
                drawingPanel.repaint();
                drawingPanel.getParent().setBackground(Color.BLACK);


            }
        });

        jpanelDraw.addMouseWheelListener(e -> {
            drawingPanel.setScale(drawingPanel.getScale() + (e.getPreciseWheelRotation() / 10));
            drawingPanel.repaint();
            drawingPanel.getParent().repaint();

        });

        SpeedB.addChangeListener(e -> {
            Ncars.setSelected(!SpeedB.isSelected());

            for (RoadSegment2D tmp : drawingPanel.getTrafficState().getRoads()) {
                tmp.ColorbySpeedofCars = SpeedB.isSelected();
                tmp.ColorbyNumberofCars = Ncars.isSelected();

            }

        });

        Ncars.addChangeListener(e -> {
            SpeedB.setSelected(!Ncars.isSelected());

            for (RoadSegment2D tmp : drawingPanel.getTrafficState().getRoads()) {
                tmp.ColorbySpeedofCars = SpeedB.isSelected();
                tmp.ColorbyNumberofCars = Ncars.isSelected();

            }
        });

        exportAsSvgButton.addActionListener(e -> {
            FileWriter writer = null;

            try {
                writer = new FileWriter("Graph" + drawingPanel.getTrafficState() + ".svg");
            } catch (IOException e1) {
                e1.printStackTrace();
            }

            String str = drawingPanel.getSvg().getSVGDocument();

            try {
                writer.write(str);
            } catch (IOException e1) {
                e1.printStackTrace();
            }

            try {
                writer.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }

            JOptionPane.showMessageDialog(null, "File Saved as " + drawingPanel.getTrafficState() + ".svg",
                    "Info", JOptionPane.CLOSED_OPTION);
        });

        comboBox1.addItemListener(e -> {


            drawingPanel.change(comboBox1.getSelectedIndex());
            drawingPanel3D.change(comboBox1.getSelectedIndex());

            ((DefaultListModel) jlistCross.getModel()).removeAllElements();
            ((DefaultListModel) jlistRoads.getModel()).removeAllElements();
            ((DefaultListModel) jlistCars.getModel()).removeAllElements();


            DefaultListModel<Cross2D> tmp = new DefaultListModel<>();

            drawingPanel.trafficState.getCrosses().forEach(x -> tmp.addElement(x));

            jlistCross.setModel(tmp);


            DefaultListModel<RoadSegment2D> tmp2 = new DefaultListModel<>();

            drawingPanel.trafficState.getRoads().forEach(x -> tmp2.addElement(x));

            jlistRoads.setModel(tmp2);


            DefaultListModel<Car> tmp3 = new DefaultListModel<>();

            for (Car a : drawingPanel.simulatorProperty().getValue().getCars())
                tmp3.addElement(a);

            jlistCars.setModel(tmp3);


        });

        jpanelDraw.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {

                super.mouseClicked(e);

                try {
                    drawingPanel.mouseClicked(drawingPanel.getMousePosition());
                } catch (NoninvertibleTransformException e1) {
                    e1.printStackTrace();
                }

            }

            @Override
            public void mousePressed(MouseEvent e) {
                super.mousePressed(e);
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                super.mouseReleased(e);
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                super.mouseEntered(e);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                super.mouseExited(e);
            }

            @Override
            public void mouseWheelMoved(MouseWheelEvent e) {
                super.mouseWheelMoved(e);
            }

            @Override
            public void mouseDragged(MouseEvent e) {
                super.mouseDragged(e);
            }

            @Override
            public void mouseMoved(MouseEvent e) {
                super.mouseMoved(e);
            }
        });

        jlistRoads.addListSelectionListener(e -> drawingPanel.setSelected((Object2D) jlistRoads.getSelectedValue()));

        jlistCross.addListSelectionListener(e -> drawingPanel.setSelected((Object2D) jlistCross.getSelectedValue()));

        jbuttonRoads.addActionListener(e -> {
            if (jscrollRoads.isVisible()) {
                jbuttonRoads.setText("+ Roads");
                jscrollRoads.setVisible(false);
            } else {
                jbuttonRoads.setText("- Roads");
                jscrollRoads.setVisible(true);
            }

        });

        jbuttoncrossesButton.addActionListener(e -> {
            if (jscrollCross.isVisible()) {
                jbuttoncrossesButton.setText("+ Crosses");
                jscrollCross.setVisible(false);
            } else {
                jbuttoncrossesButton.setText("- Crosses");
                jscrollCross.setVisible(true);
            }

        });

        jbuttonCars.addActionListener(e -> {
            if (jscrollCars.isVisible()) {
                jbuttonCars.setText("+ Cars");
                jscrollCars.setVisible(false);
            } else {
                jbuttonCars.setText("- Cars");
                jscrollCars.setVisible(true);
            }


        });

        jpanelDraw.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                super.componentResized(e);
            }
        });


        slider1.addChangeListener(e -> {
            delay = slider1.getValue();


            jlabelSlider.setText("Delay: " + dec.format(delay) + "ms");

            timer.setDelay((int) delay);

        });

        slider2.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {

                next = ((double) slider2.getValue());

                jlabelSlidej2.setText("NextStep: " + dec.format(next) + "ms");

            }
        });

        trafficVisibilityRadioButton.addChangeListener(e -> {

            if (trafficVisibilityRadioButton.isSelected())
                for (Cross2D tmp : drawingPanel.getTrafficState().getCrosses())
                    for (TrafficLight2D tmp2 : tmp.getLights())
                        tmp2.setVisible(true);

            if (!trafficVisibilityRadioButton.isSelected())
                for (Cross2D tmp : drawingPanel.getTrafficState().getCrosses())
                    for (TrafficLight2D tmp2 : tmp.getLights())
                        tmp2.setVisible(false);


        });

        ResetAllGraph.addActionListener(e -> {
            graph.setData(new ArrayList<>());
            graph.setMaxX(0);
            graph.setMaxY(0);
        });

        Xtickplus.addActionListener(e -> graph.setTickX(graph.getTickX() + 1));

        xTickminus.addActionListener(e -> graph.setTickX(graph.getTickX() - 1));

        Ytickminus.addActionListener(e -> graph.setTickY(graph.getTickY() - 1));

        Ytickplus.addActionListener(e -> graph.setTickY(graph.getTickY() + 1));

        movingAvgCheckBox.addChangeListener(e -> {
            graph.setMovingAvg(movingAvgCheckBox.isSelected());


            spinner1.setEnabled(movingAvgCheckBox.isSelected());
        });

        spinner1.addChangeListener(e -> {

            spinner1.setValue(spinner1.getValue());

            if ((int) spinner1.getValue() <= 0)
                spinner1.setValue(1);

            graph.setMovingAvgNum((int) spinner1.getValue());

        });

        spinner1.setValue(5);

        ExportgrafData.addActionListener(e -> {
            FileWriter writer = null;

            try {
                writer = new FileWriter("Graph" + graph.getData().size() + ".txt");
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            for (Point2D tmp : graph.getData()) {
                String str = tmp.getX() + "," + tmp.getY() + ";\n";

                try {
                    writer.write(str);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
            try {
                writer.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }

            JOptionPane.showMessageDialog(null, "File Saved as " + "Graph" + graph.getData().size() + ".txt",
                    "Info", JOptionPane.CLOSED_OPTION);

        });

        ExportgrafSvg.addActionListener(e -> {
            FileWriter writer = null;

            try {
                writer = new FileWriter("Graph" + graph.getData().size() + ".svg");
            } catch (IOException e1) {
                e1.printStackTrace();
            }

            String str = graph.getSvg().getSVGDocument();

            try {
                writer.write(str);
            } catch (IOException e1) {
                e1.printStackTrace();
            }

            try {
                writer.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }

            JOptionPane.showMessageDialog(null, "File Saved as " + graph.getData().size() + ".svg",
                    "Info", JOptionPane.CLOSED_OPTION);

        });


    }

    /**
     *
     */
    public void createJlist() {


        jlistCross = new JList();

        DefaultListModel<Cross2D> tmp = new DefaultListModel<>();

        drawingPanel.trafficState.getCrosses().forEach(x -> tmp.addElement(x));

        jlistCross.setModel(tmp);

        jlistRoads = new JList();

        DefaultListModel<RoadSegment2D> tmp2 = new DefaultListModel<>();

        drawingPanel.trafficState.getRoads().forEach(x -> tmp2.addElement(x));

        jlistRoads.setModel(tmp2);


        jlistCars = new JList();

        DefaultListModel<Car> tmp3 = new DefaultListModel<>();

        for (Car a : drawingPanel.simulatorProperty().getValue().getCars())
            tmp3.addElement(a);

        jlistCars.setModel(tmp3);


        jlistCross.addListSelectionListener(e -> {
            jlistRoads.clearSelection();
            jlistCars.clearSelection();
        });

        jlistRoads.addListSelectionListener(e -> {
            jlistCross.clearSelection();
            jlistCars.clearSelection();
        });

        jlistCars.addListSelectionListener(e -> {
            jlistCross.clearSelection();
            jlistRoads.clearSelection();
        });


    }


    /**
     *
     */
    public void createJdraw() {

        drawingPanel = new DrawingPanel(Integer.parseInt(arg));

        drawingPanel3D = new DrawingPanel3D(Integer.parseInt(arg));


        model3D = drawingPanel3D;

        jpanelDraw = drawingPanel;

        relaxBrick = new RelaxingCube3Dsimulator();

        drawingPanel.addListenerIClickedObject(this);

    }


    /**
     *
     */
    private void createUIComponents() {

        createJdraw();

        createJlist();

        graph = new Graph("[s]", "[cars]");

        Graph1 = graph;

        comboBox1 = new JComboBox<>(drawingPanel.simulatorProperty().getValue().getScenarios());

        comboBox1.setSelectedIndex(Integer.parseInt(arg));


    }

    /**
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {


        drawingPanel.getSimulator().nextStep((next / 1000));

        drawingPanel.repaint();

        tmpN += (next / 1000);

        graph.getData().add(new Point2D.Double(tmpN, drawingPanel.getSimulator().getCarsPerHour()));

        graph.repaint();

        back.repaint();

        split.repaint();


        for (RoadSegment2D tmp : drawingPanel.getTrafficState().getRoads())
            for (Lane2D tmp2 : tmp.getLanes())
                tmp2.update((next / 1000));


    }

    /**
     * @param object2D
     * @throws NoninvertibleTransformException
     */
    @Override
    public void clickedObject(Object2D object2D) throws NoninvertibleTransformException {


        if (object2D instanceof RoadSegment2D) {

            jlistRoads.setSelectedIndex(drawingPanel.getTrafficState().getRoads().indexOf(object2D));
        }
        if (object2D instanceof Cross2D) {

            jlistCross.setSelectedIndex(drawingPanel.trafficState.getCrosses().indexOf(object2D));

        }
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        createUIComponents();
        MainPanel = new JPanel();
        MainPanel.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(2, 2, new Insets(0, 0, 0, 0), -1, -1));
        MainPanel.setBackground(new Color(-9538952));
        final JToolBar toolBar1 = new JToolBar();
        MainPanel.add(toolBar1, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 2, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(-1, 20), null, 0, false));
        final JPanel panel1 = new JPanel();
        panel1.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        toolBar1.add(panel1);
        panel1.add(comboBox1, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_EAST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, new Dimension(640, 30), null, 0, false));
        final JSplitPane splitPane1 = new JSplitPane();
        splitPane1.setDividerLocation(423);
        splitPane1.setDividerSize(5);
        splitPane1.setResizeWeight(1.0);
        MainPanel.add(splitPane1, new com.intellij.uiDesigner.core.GridConstraints(1, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, null, new Dimension(200, 200), null, 0, false));
        final JSplitPane splitPane2 = new JSplitPane();
        splitPane2.setDividerLocation(119);
        splitPane2.setDividerSize(5);
        splitPane1.setLeftComponent(splitPane2);
        jpanelTab = new JPanel();
        jpanelTab.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(6, 1, new Insets(0, 0, 0, 0), -1, -1));
        jpanelTab.setBackground(new Color(-4276546));
        jpanelTab.setForeground(new Color(-4276546));
        splitPane2.setLeftComponent(jpanelTab);
        jbuttoncrossesButton = new JButton();
        jbuttoncrossesButton.setBackground(new Color(-14276823));
        jbuttoncrossesButton.setForeground(new Color(-1184275));
        jbuttoncrossesButton.setHideActionText(false);
        jbuttoncrossesButton.setHorizontalAlignment(2);
        jbuttoncrossesButton.setHorizontalTextPosition(2);
        jbuttoncrossesButton.setText("- Crosses");
        jpanelTab.add(jbuttoncrossesButton, new com.intellij.uiDesigner.core.GridConstraints(2, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_NORTH, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        jbuttonCars = new JButton();
        jbuttonCars.setBackground(new Color(-15592941));
        jbuttonCars.setForeground(new Color(-723724));
        jbuttonCars.setHorizontalAlignment(2);
        jbuttonCars.setHorizontalTextPosition(2);
        jbuttonCars.setText("- Cars");
        jpanelTab.add(jbuttonCars, new com.intellij.uiDesigner.core.GridConstraints(4, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_NORTH, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        jbuttonRoads = new JButton();
        jbuttonRoads.setBackground(new Color(-14276823));
        jbuttonRoads.setEnabled(true);
        jbuttonRoads.setForeground(new Color(-263173));
        jbuttonRoads.setHorizontalAlignment(2);
        jbuttonRoads.setHorizontalTextPosition(2);
        jbuttonRoads.setText("- Roads");
        jpanelTab.add(jbuttonRoads, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_NORTH, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        jscrollRoads = new JScrollPane();
        jpanelTab.add(jscrollRoads, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        jscrollRoads.setViewportView(jlistRoads);
        jscrollCross = new JScrollPane();
        jpanelTab.add(jscrollCross, new com.intellij.uiDesigner.core.GridConstraints(3, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        jlistCross.setForeground(new Color(-15592941));
        jscrollCross.setViewportView(jlistCross);
        jscrollCars = new JScrollPane();
        jpanelTab.add(jscrollCars, new com.intellij.uiDesigner.core.GridConstraints(5, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        jscrollCars.setViewportView(jlistCars);
        split = new JSplitPane();
        split.setDividerLocation(337);
        split.setDividerSize(5);
        split.setOrientation(0);
        split.setResizeWeight(1.0);
        splitPane2.setRightComponent(split);
        final JPanel panel2 = new JPanel();
        panel2.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        split.setRightComponent(panel2);
        info = new JTextPane();
        info.setEditable(false);
        panel2.add(info, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, null, new Dimension(150, 50), null, 0, false));
        final JPanel panel3 = new JPanel();
        panel3.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
        split.setLeftComponent(panel3);
        back = new JPanel();
        back.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel3.add(back, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        jpanelDraw.setBackground(new Color(-4276546));
        back.add(jpanelDraw, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JToolBar toolBar2 = new JToolBar();
        panel3.add(toolBar2, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(-1, 20), null, 0, false));
        exportAsSvgButton = new JButton();
        exportAsSvgButton.setText("Export as .svg");
        toolBar2.add(exportAsSvgButton);
        jpanelList = new JPanel();
        jpanelList.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
        jpanelList.setBackground(new Color(-9538952));
        jpanelList.setEnabled(true);
        jpanelList.putClientProperty("html.disable", Boolean.FALSE);
        splitPane1.setRightComponent(jpanelList);
        final JPanel panel4 = new JPanel();
        panel4.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel4.setBackground(new Color(-4276546));
        jpanelList.add(panel4, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        tabbedPane1 = new JTabbedPane();
        panel4.add(tabbedPane1, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, new Dimension(200, 200), null, 0, false));
        final JPanel panel5 = new JPanel();
        panel5.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(3, 1, new Insets(0, 0, 0, 0), -1, -1));
        tabbedPane1.addTab("Cars per hours graph", panel5);
        final JToolBar toolBar3 = new JToolBar();
        panel5.add(toolBar3, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(-1, 20), null, 0, false));
        Ytickplus = new JButton();
        Ytickplus.setText("Add Y tick");
        toolBar3.add(Ytickplus);
        Ytickminus = new JButton();
        Ytickminus.setText("Remove Y tick");
        toolBar3.add(Ytickminus);
        Xtickplus = new JButton();
        Xtickplus.setText("Add X tick");
        toolBar3.add(Xtickplus);
        xTickminus = new JButton();
        xTickminus.setText("Remove X tick");
        toolBar3.add(xTickminus);
        panel5.add(Graph1, new com.intellij.uiDesigner.core.GridConstraints(2, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JToolBar toolBar4 = new JToolBar();
        panel5.add(toolBar4, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(-1, 20), null, 0, false));
        ResetAllGraph = new JButton();
        ResetAllGraph.setText("Reset All");
        toolBar4.add(ResetAllGraph);
        movingAvgCheckBox = new JCheckBox();
        movingAvgCheckBox.setText("Moving Avg");
        toolBar4.add(movingAvgCheckBox);
        spinner1 = new JSpinner();
        toolBar4.add(spinner1);
        ExportgrafData = new JButton();
        ExportgrafData.setText("Export as .txt");
        toolBar4.add(ExportgrafData);
        ExportgrafSvg = new JButton();
        ExportgrafSvg.setText("Export as .svg");
        toolBar4.add(ExportgrafSvg);
        final JPanel panel6 = new JPanel();
        panel6.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        tabbedPane1.addTab("3D model", panel6);
        final JPanel panel7 = new JPanel();
        panel7.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel6.add(panel7, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        panel7.add(model3D, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JPanel panel8 = new JPanel();
        panel8.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        tabbedPane1.addTab("Relax brick", panel8);
        panel8.add(relaxBrick, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JPanel panel9 = new JPanel();
        panel9.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(3, 4, new Insets(0, 0, 0, 0), -1, -1));
        panel9.setBackground(new Color(-4276546));
        jpanelList.add(panel9, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, new Dimension(-1, 100), null, new Dimension(-1, 100), 0, false));
        slider1 = new JSlider();
        slider1.setBackground(new Color(-4276546));
        slider1.setForeground(new Color(-15592941));
        slider1.setInverted(false);
        slider1.setMajorTickSpacing(1);
        slider1.setMaximum(1000);
        slider1.setMinimum(1);
        slider1.setMinorTickSpacing(1);
        slider1.setValue(1);
        panel9.add(slider1, new com.intellij.uiDesigner.core.GridConstraints(0, 1, 1, 3, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        jlabelSlider = new JLabel();
        jlabelSlider.setHorizontalAlignment(2);
        jlabelSlider.setHorizontalTextPosition(2);
        jlabelSlider.setText("Label");
        panel9.add(jlabelSlider, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        jlabelSlidej2 = new JLabel();
        jlabelSlidej2.setText("Label");
        panel9.add(jlabelSlidej2, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, new Dimension(50, 50), null, null, 0, false));
        slider2 = new JSlider();
        slider2.setBackground(new Color(-4276546));
        slider2.setForeground(new Color(-15592941));
        slider2.setMaximum(1000);
        slider2.setMinimum(1);
        slider2.setValue(1);
        panel9.add(slider2, new com.intellij.uiDesigner.core.GridConstraints(1, 1, 1, 3, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        trafficVisibilityRadioButton = new JRadioButton();
        trafficVisibilityRadioButton.setBackground(new Color(-4276546));
        trafficVisibilityRadioButton.setForeground(new Color(-14276823));
        trafficVisibilityRadioButton.setText("Trafficlight visibility");
        panel9.add(trafficVisibilityRadioButton, new com.intellij.uiDesigner.core.GridConstraints(2, 0, 1, 2, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        Ncars = new JRadioButton();
        Ncars.setBackground(new Color(-4276546));
        Ncars.setForeground(new Color(-14671840));
        Ncars.setText("Colors by number");
        panel9.add(Ncars, new com.intellij.uiDesigner.core.GridConstraints(2, 3, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        SpeedB = new JRadioButton();
        SpeedB.setBackground(new Color(-4276546));
        SpeedB.setForeground(new Color(-14671840));
        SpeedB.setText("Colors by speed");
        panel9.add(SpeedB, new com.intellij.uiDesigner.core.GridConstraints(2, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return MainPanel;
    }
}
