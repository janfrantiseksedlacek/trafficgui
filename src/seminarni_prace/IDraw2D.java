package seminarni_prace;

import java.awt.*;
import java.awt.geom.Point2D;

/**
 *
 */
public interface IDraw2D
{
    /**
     * @param g
     */
    void draw(Graphics2D g);

    /**
     * @param parent
     */
    void calculateSegments(Object parent);

    /**
     * @param start
     * @param end
     * @return
     */
    double getAngle(Point2D start, Point2D end);

}
