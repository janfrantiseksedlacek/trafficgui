package seminarni_prace;

import TrafficSim.RoadSegment;
import javafx.beans.property.SimpleObjectProperty;

import java.awt.*;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class RoadSegment2D extends Object2D{

    /**
     *
     */
    private SimpleObjectProperty<List<Lane2D>>Lanes=new SimpleObjectProperty<>();

    /**
     *
     */
    private SimpleObjectProperty<Polygon>HitBox= new SimpleObjectProperty<>(new Polygon());

    /**
     *
     */
    private SimpleObjectProperty<RoadSegment> ParentRoad=new SimpleObjectProperty<>();

    /**
     *
     */
    public boolean ColorbyNumberofCars = false;

    /**
     *
     */
    public boolean ColorbySpeedofCars = true;

    /**
     * @param road
     */
    public RoadSegment2D(RoadSegment road) {


        setLanes(new ArrayList<>());

        setParentRoad(road);

        calculateSegments(road);


    }


    /**
     * @return
     */
    public List<Lane2D> getLanes() {
        return Lanes.get();
    }

    /**
     * @return
     */
    public SimpleObjectProperty<List<Lane2D>> lanesProperty() {
        return Lanes;
    }

    /**
     * @param lanes
     */
    public void setLanes(List<Lane2D> lanes) {
        this.Lanes.set(lanes);
    }

    /**
     * @return
     */
    public Polygon getHitBox() {
        return HitBox.get();
    }

    /**
     * @return
     */
    public SimpleObjectProperty<Polygon> hitBoxProperty() {
        return HitBox;
    }

    /**
     * @param hitBox
     */
    public void setHitBox(Polygon hitBox) {
        this.HitBox.set(hitBox);
    }

    /**
     * @return
     */
    public RoadSegment getParentRoad() {
        return ParentRoad.get();
    }

    /**
     * @return
     */
    public SimpleObjectProperty<RoadSegment> parentRoadProperty() {
        return ParentRoad;
    }

    /**
     * @param parentRoad
     */
    public void setParentRoad(RoadSegment parentRoad) {
        this.ParentRoad.set(parentRoad);
    }


    /**
     * @param time
     */
    public void update(double time) {
        for (Lane2D tmp : getLanes())
            tmp.update(time);

    }

    /**
     * @param g
     */
    @Override
    public void draw(Graphics2D g) {
        {


            for (Lane2D tmp : getLanes())
            {

                if (!getSelected()) {

                    if (ColorbyNumberofCars) {
                        double curr = tmp.getParentLane().getNumberOfCarsCurrent();
                        double max = (Point2D.distance(tmp.getLine().getX1(), tmp.getLine().getY1(), tmp.getLine().getX2(), tmp.getLine().getY2())) / 15;
                        double norm = curr / max;

                        if (norm > 1)
                            norm = 1;

                        setColor(new Color((int) (norm * 250), 10, 10).brighter());
                    } else if (ColorbySpeedofCars) {
                        double curr = tmp.getParentLane().getSpeedAverage();
                        double max = tmp.getParentLane().getSpeedLimit();

                        double norm = curr / max;


                        if (norm > 1)
                            norm = 1;

                        if (curr == 0)
                            norm = 0;


                        setColor(new Color(10, (int) (250 * norm), 10).brighter());
                    }
                }
                else
                    setColor(Color.BLUE);

                g.setColor(getColor());

                g.setStroke(new BasicStroke(2));

                g.draw(tmp.getLine());



            }

        }
    }

    /**
     * @param parent
     */
    @Override
    public void calculateSegments(Object parent) {

        RoadSegment road=(RoadSegment)parent;

        //=====================
        //Promnene
        //=====================
        Point2D start = road.getStartPosition();
        Point2D end = road.getEndPosition();
        double forward = road.getForwardLanesCount();
        double backward = road.getBackwardLanesCount();
        double sep = road.getLaneSeparatorWidth();
        double lineSize = road.getLaneWidth();
        //=====================

        //=====================
        // Sin a Cos pro posuny
        //=====================
        double ang = getAngle(start, end);
        double sin = Math.sin(Math.toRadians(ang)) * -1;
        double cos = Math.cos(Math.toRadians(ang)) * -1;
        //=====================



        //========================================
        //  Rovnice
        //
        //  X[i]= X[i-1] +- sirkaSilnice * sin
        //  Y[i]= Y[i-1] +- sirkaSilnice * cos
        //========================================

        for (int i = 0; i < forward; i++) {



            getLanes().add(new Lane2D(road.getLane(i+1),new Line2D.Double(start,end)));
            HitBox.getValue().addPoint((int)start.getX(), (int)start.getY());
            HitBox.getValue().addPoint((int)end.getX(), (int)end.getY());

            start.setLocation(start.getX() + lineSize * sin, start.getY() - lineSize * cos);

            end.setLocation(end.getX() + lineSize * sin, end.getY() - lineSize * cos);

        }

        //=============================================
        //Posunuti "kreslitka" na druhou stranu silnice
        //=============================================

        start = road.getStartPosition();
        end = road.getEndPosition();

        start.setLocation(start.getX() - sep * sin, start.getY() + sep * cos);

        end.setLocation(end.getX() - sep * sin, end.getY() + sep * cos);

        start.setLocation(start.getX() - lineSize * sin, start.getY() + lineSize * cos);

        end.setLocation(end.getX() - lineSize * sin, end.getY() + lineSize * cos);

        //=============================================

        for (double i = backward; i > 0; i--) {

            getLanes().add(new Lane2D(road.getLane(-(int)(backward-i)-1),new Line2D.Double(start,end)));
            HitBox.getValue().addPoint((int)start.getX(), (int)start.getY());
            HitBox.getValue().addPoint((int)end.getX(), (int)end.getY());

            start.setLocation(start.getX() - lineSize * sin, start.getY() + lineSize * cos);

            end.setLocation(end.getX() - lineSize * sin, end.getY() + lineSize * cos);

        }


    }


    /**
     * @return
     */
    @Override
    public String toString() {
        return "" +
                "Name: " + ParentRoad.getValue().getId() +
                ", Lanes: " + Lanes.getValue().size();
    }
}