package seminarni_prace;

import TrafficSim.TrafficLight;
import TrafficSim.TrafficLightState;
import javafx.beans.property.SimpleObjectProperty;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;

/**
 *
 */
public class TrafficLight2D extends Object2D {

    /**
     * @return
     */
    public boolean isVisible() {
        return isVisible;

    }

    /**
     * @param visible
     */
    public void setVisible(boolean visible) {
        isVisible = visible;
    }

    /**
     *
     */
    private boolean isVisible=true;

    /**
     * @param parentTraff
     * @param pozice
     */
    public TrafficLight2D(TrafficLight parentTraff, Point2D pozice) {
        this.parentTraff.set( parentTraff);
        this.setEllipse(new Ellipse2D.Double(pozice.getX(),pozice.getY(),2,2));
    }

    /**
     * @param parentTraff
     */
    public TrafficLight2D(TrafficLight parentTraff)
    {
        this.parentTraff.set( parentTraff);
        this.setEllipse(new Ellipse2D.Double(0,0,2,2));
    }

    /**
     * @return
     */
    public TrafficLight getParentTraff() {
        return parentTraff.get();
    }

    /**
     * @return
     */
    public SimpleObjectProperty<TrafficLight> parentTraffProperty() {
        return parentTraff;
    }

    /**
     * @param parentTraff
     */
    public void setParentTraff(TrafficLight parentTraff) {
        this.parentTraff.set(parentTraff);
    }

    /**
     * @return
     */
    public Ellipse2D getEllipse() {
        return ellipse.get();
    }

    /**
     * @return
     */
    public SimpleObjectProperty<Ellipse2D> ellipseProperty() {
        return ellipse;
    }

    /**
     * @param ellipse
     */
    public void setEllipse(Ellipse2D ellipse) {
        this.ellipse.set(ellipse);
    }

    /**
     *
     */
    private SimpleObjectProperty<TrafficLight> parentTraff = new SimpleObjectProperty<>();

    /**
     *
     */
    private SimpleObjectProperty<Ellipse2D> ellipse = new SimpleObjectProperty<>();

    /**
     * @param g
     */
    @Override
    public void draw(Graphics2D g) {


        g.setColor(Color.DARK_GRAY);

        g.fill(new Ellipse2D.Double(getEllipse().getX()-1,getEllipse().getY()-1,5,5));

            g.setColor(Color.ORANGE);

        if (getParentTraff().getCurrentState().equals(TrafficLightState.GO))
            g.setColor(Color.GREEN);

        if (getParentTraff().getCurrentState().equals(TrafficLightState.STOP))
            g.setColor(Color.RED);



        g.fill(getEllipse());
    }

    /**
     * @param parent
     */
    @Deprecated
    @Override
    public void calculateSegments(Object parent)
    {


    }
}
