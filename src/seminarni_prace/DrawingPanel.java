package seminarni_prace;


import TrafficSim.Car;
import TrafficSim.Simulator;
import javafx.beans.property.SimpleObjectProperty;
import org.jfree.graphics2d.svg.SVGGraphics2D;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Line2D;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;
import java.util.ArrayList;


/**
 *
 */
public class DrawingPanel extends JPanel  {


    /**
     *
     */
    TrafficState trafficState;

    /**
     * @return
     */
    public TrafficState getTrafficState() {
        return trafficState;
    }

    /**
     * "The serialization runtime associates with each serializable class a version number, called a serialVersionUID"
     * {@linkplain "https://stackoverflow.com/questions/285793/what-is-a-serialversionuid-and-why-should-i-use-it" }
     */
    private static final long serialVersionUID = 1L;


    /**
     * Barva aut
     */
    private Color CarColor =new Color(241, 241, 241);


    /**
     *
     */
    private ArrayList<IClickedOnObject> iClickedOnObjects =new ArrayList<>();


    /**
     * @return
     */
    public SimpleObjectProperty<Simulator> simulatorProperty() {
        return simulator;
    }


    /**
     * Prehravana simulace
     */
    private SimpleObjectProperty<Simulator> simulator = new SimpleObjectProperty<>();


    /**
     * Konstruktor
     * @param scenar ID scenare
     */
    public DrawingPanel(int scenar) {

        super();

        simulator.setValue(new Simulator());

        String[] scenarios = simulator.getValue().getScenarios();

        simulator.getValue().runScenario(scenarios[scenar]);

        trafficState=new TrafficState(simulatorProperty().getValue().getRoadSegments(), simulatorProperty().getValue().getCrossroads());





    }

    /**
     * @param g
     */
    public  void computeModelDimensions(Graphics2D g) {

        double MinX=0;
        double MinY=0;
        double MaxX=0;
        double MaxY=0;

        for (Point2D tmp:getTrafficState().getAllPoints())
        {

            if (tmp.getX() > MaxX)
                MaxX = tmp.getX();

            if (tmp.getY() > MaxY)
                MaxY = tmp.getY();

            if (tmp.getX() < MinX)
                MinX = tmp.getX();

            if (tmp.getY() < MinY)
                MinY = tmp.getY();

        }


        double Width = ( Math.abs(MaxX - MinX))+1;

        double Height = ( Math.abs(MaxY - MinY))+1;

        double scaleX=this.getWidth()/Width;

        double scaleY=this.getHeight()/Height;


        g.translate(-MinX*scaleX, -MinY*scaleY);

        double scale= Math.min(scaleX, scaleY);

        g.scale(scale, scale);




    }

    /**
     * @param g
     * @return
     */
    public Graphics2D computeg2(Graphics g)
    {


        Graphics2D g2 = (Graphics2D) g;

        computeModelDimensions(g2);

        return g2;

    }

    /**
     * @param g
     * @return
     */
    public SVGGraphics2D computeg2Svg(SVGGraphics2D g) {


        SVGGraphics2D g2 = g;

        computeModelDimensions(g2);

        return g2;

    }


    /**
     * @return
     */
    public SVGGraphics2D getSvg() {


        SVGGraphics2D g3 = new SVGGraphics2D(getWidth(), getHeight());

        g3.translate(0 + getTranX(), getHeight() + getTranY());

        g3.scale(scale, scale);


        g3.scale(1, -1);


        SVGGraphics2D g2 = computeg2Svg(g3);


        RenderingHints rh = new RenderingHints(
                RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        g2.setRenderingHints(rh);


        getTrafficState().draw(g2);

        drawCars(getSimulator().getCars(), g2);


        return g2;


    }

    /**
     * @return
     */
    public int getTranX() {
        return tranX;
    }

    /**
     * @param tranX
     */
    public void setTranX(int tranX) {
        this.tranX = tranX;
    }

    /**
     * @return
     */
    public int getTranY() {
        return tranY;
    }

    /**
     * @param tranY
     */
    public void setTranY(int tranY) {
        this.tranY = tranY;
    }

    /**
     *
     */
    private int tranX=0;
    /**
     *
     */
    private int tranY=0;

    /**
     * @return
     */
    public double getScale() {
        return scale;
    }

    /**
     * @param scale
     */
    public void setScale(double scale) {
        this.scale = scale;

    }

    /**
     *
     */
    private double scale=1;


    /**
     * Vykresli compenenty
     * @param g graficky kontent
     */
    @Override
    public void paintComponent(Graphics g) {


        setSize(getParent().getSize());

        Graphics2D g3 = (Graphics2D) g;

        g3.translate(0+getTranX(), getHeight()+getTranY());

        g3.scale(scale,scale);


        g3.scale(1, -1);



        super.paintComponent(g);

        Graphics2D g2 = computeg2(g);


        RenderingHints rh = new RenderingHints(
                RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        g2.setRenderingHints(rh);




        getTrafficState().draw(g2);

        drawCars(getSimulator().getCars(), g2);





    }


    /**
     *
     */
    public void updateTraff()
    {

        trafficState=new TrafficState(simulatorProperty().getValue().getRoadSegments(), simulatorProperty().getValue().getCrossroads());


    }


    /**
     * @param e
     * @return
     * @throws NoninvertibleTransformException
     */
    public String getMessege(Point2D e) throws NoninvertibleTransformException {

        messege="";

        Polygon y=new Polygon();


       AffineTransform a= new AffineTransform();





        if(e!=null) {
            e.setLocation(e.getX()-tranX, (this.getHeight() - e.getY())+tranY);

            e.setLocation(e.getX()/scale,  e.getY()/scale);

            y.addPoint((int) ((e.getX()- 10)), (int) ((e.getY() - 10)));

            y.addPoint((int) ((e.getX() + 10)), (int) ((e.getY() + 10)));




        messege+="Position: "+y.getBounds().getX()+" ; "+y.getBounds().getY()+"\n";

        for (Cross2D tmp:getTrafficState().getCrosses())
            if (tmp.getHitBox().intersects(computeg2(this.getGraphics()).getTransform().createInverse().createTransformedShape(y).getBounds2D()))
                messege += "Object: " + tmp + "\n";

            for (RoadSegment2D tmp : getTrafficState().getRoads())
            if (tmp.getHitBox().intersects(computeg2(this.getGraphics()).getTransform().createInverse().createTransformedShape(y).getBounds2D()))
                messege += "Object: " + tmp + "\n";

        }
        return messege;
    }

    /**
     * @param messege
     */
    public void setMessege(String messege) {
        this.messege = messege;
    }

    /**
     *
     */
    private String messege="";

    /**
     * @param i
     */
    public void change(int i)
    {
        simulator.setValue(new Simulator());

        String[] scenarios = simulator.getValue().getScenarios();

        simulatorProperty().getValue().clear();

        simulator.getValue().runScenario(scenarios[i]);

        trafficState=new TrafficState(simulatorProperty().getValue().getRoadSegments(), simulatorProperty().getValue().getCrossroads());

        scale=1;
        tranY=0;
        tranX=0;


    }


    /**
     * Vykresli jedno auto
     * @param car auto
     * @param g graficky kontent
     */
    void drawCar(Car car, Graphics2D g) {

        Line2D line = new Line2D.Double(car.getPosition().getX(), car.getPosition().getY(),
                car.getPosition().getX() + car.getLength() / 4, car.getPosition().getY());

        AffineTransform at = AffineTransform.getRotateInstance(car.getOrientation(), line.getX1(), line.getY1());

        g.setStroke(new BasicStroke(1));
        g.setColor(CarColor);
        g.draw(at.createTransformedShape(line));

    }


    /**
     * Vykresli pole aut
     * @param cars auta
     * @param g graficky kontent
     */
    void drawCars(Car[] cars, Graphics2D g) {

        for (Car tmp : cars)
           drawCar(tmp, g);

    }


    /**
     * @param toAdd
     */
    public void addListenerIClickedObject(IClickedOnObject toAdd)

    {
        iClickedOnObjects.add(toAdd);
    }


    /**
     * Simulator
     * @return Simulator
     */
    public Simulator getSimulator()
    {
        return simulator.get();
    }


    /**
     * @param object2D
     */
    public void setSelected(Object2D object2D)
    {

        getTrafficState().unSelectedAll();

        if (object2D!=null)
        {
            object2D.setSelected(true);

        }


    }


    /**
     * @param e
     * @throws NoninvertibleTransformException
     */
    public void mouseClicked(Point2D e) throws NoninvertibleTransformException
    {

        getTrafficState().unSelectedAll();

        Object2D clicke =getClickedObject(e);

        if (clicke!=null)
        {
            clicke.setSelected(true);


            for (IClickedOnObject listener : iClickedOnObjects)
                listener.clickedObject(clicke);
        }


    }


    /**
     * @param e
     * @return
     * @throws NoninvertibleTransformException
     */
    public Object2D getClickedObject(Point2D e) throws NoninvertibleTransformException {

        Polygon y=new Polygon();

        if(e!=null) {
            e.setLocation(e.getX()-tranX, (this.getHeight() - e.getY())+tranY);

            e.setLocation(e.getX()/scale,  e.getY()/scale);

            y.addPoint((int) e.getX()- 10, (int) e.getY() - 10);

            y.addPoint((int) e.getX() + 10, (int) e.getY() + 10);
        }

        for (RoadSegment2D tmp : getTrafficState().getRoads())
            for (Lane2D tmp2 : tmp.getLanes())
                if (tmp2.getLine().intersects(computeg2(this.getGraphics()).getTransform().createInverse().createTransformedShape(y).getBounds2D())) {
                    JFrame frame = new JFrame(tmp2.getParentLane().toString());
                    frame.setContentPane(new Graph("[s]", "[Total of Cars]", tmp2.getData()));
                    frame.setSize(250, 250);
                    frame.show();

                    frame.repaint();


                }

        for (Cross2D tmp:getTrafficState().getCrosses())
            if (tmp.getHitBox().intersects(computeg2(this.getGraphics()).getTransform().createInverse().createTransformedShape(y).getBounds2D()))
              return tmp;

        for (RoadSegment2D tmp:getTrafficState().getRoads())
            if (tmp.getHitBox().intersects(computeg2(this.getGraphics()).getTransform().createInverse().createTransformedShape(y).getBounds2D()))
                return tmp;


        return null;


    }






}
