package seminarni_prace;

import TrafficSim.Lane;

import java.awt.*;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;

/**
 *
 */
public class Lane2D extends Object2D {

    /**
     *
     */
    private Lane ParentLane;

    /**
     *
     */
    private Line2D Line;

    /**
     *
     */
    private ArrayList<Point2D> data = new ArrayList<>();

    /**
     *
     */
    private double currentTimeSeconds = 0;

    /**
     *
     */
    private Color color = Color.BLACK;

    /**
     * @return
     */
    public ArrayList<Point2D> getData() {
        return data;
    }

    /**
     * @param data
     */
    public void setData(ArrayList<Point2D> data) {
        this.data = data;
    }

    /**
     * @param time
     */
    public void update(double time) {

        data.add(new Point2D.Double(currentTimeSeconds + time, getParentLane().getNumberOfCarsTotal()));

        currentTimeSeconds += time;
    }

    /**
     * @return
     */
    @Override
    public Color getColor() {
        return color;
    }

    /**
     * @param color
     */
    @Override
    public void setColor(Color color) {
        this.color = color;
    }

    /**
     * @param parentLane
     * @param line
     */
    public Lane2D(Lane parentLane, Line2D line) {
        ParentLane = parentLane;
        Line = line;
    }

    /**
     * @return
     */
    public Lane getParentLane() {
        return ParentLane;
    }

    /**
     * @param parentLane
     */
    public void setParentLane(Lane parentLane) {
        ParentLane = parentLane;
    }

    /**
     * @return
     */
    public Line2D getLine() {
        return Line;
    }

    /**
     * @param line
     */
    public void setLine(Line2D line) {
        Line = line;
    }

    /**
     * @param g
     */
    @Override
    public void draw(Graphics2D g) {
        g.draw(Line);
    }

    /**
     * @param parent
     */
    @Override
    public void calculateSegments(Object parent) {

    }
}
