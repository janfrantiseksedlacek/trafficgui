package seminarni_prace;

import TrafficSim.CrossRoad;
import TrafficSim.RoadSegment;
import javafx.beans.property.SimpleObjectProperty;

import java.awt.*;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class TrafficState3D {

    /**
     *
     */
    private SimpleObjectProperty<List<RoadSegment3D>> roads = new SimpleObjectProperty<>(new ArrayList<>());

    /**
     *
     */
    private SimpleObjectProperty<List<Cross3D>> crosses = new SimpleObjectProperty<>(new ArrayList<>());

    /**
     *
     */
    private ArrayList<Point2D> point2DS = new ArrayList<>();

    /**
     *
     */
    private Line3D x = new Line3D(new Point3D(-250, 0, 0), new Point3D(250, 0, 0), 0);
    /**
     *
     */
    private Line3D y = new Line3D(new Point3D(0, -250, 0), new Point3D(0, 250, 0), 0);
    /**
     *
     */
    private Line3D z = new Line3D(new Point3D(0, 0, -250), new Point3D(0, 0, 250), 0);
    /**
     *
     */
    private SimpleObjectProperty<Boolean> isVisible = new SimpleObjectProperty<>(true);

    /**
     * @param roads
     * @param crosses
     */
    public TrafficState3D(RoadSegment[] roads, CrossRoad[] crosses) {
        ArrayList<Cross3D> tmpC = new ArrayList<>();

        ArrayList<RoadSegment3D> tmpR = new ArrayList<>();

        for (CrossRoad c : crosses) {
            tmpC.add(new Cross3D(c));

        }

        for (RoadSegment c : roads) {
            tmpR.add(new RoadSegment3D(c));

        }

        setCrosses(tmpC);

        setRoads(tmpR);

        for (Cross3D tmp : getCrosses()) {
            for (Lane3D tmp1 : tmp.getLanes()) {

                point2DS.add(tmp1.getLine().getP1().toPoint2D());
                point2DS.add(tmp1.getLine().getP2().toPoint2D());

            }
        }

        for (RoadSegment3D tmp : getRoads()) {
            for (Lane3D tmp1 : tmp.getLanes()) {

                point2DS.add(tmp1.getLine().getP1().toPoint2D());
                point2DS.add(tmp1.getLine().getP2().toPoint2D());

            }
        }


    }

    /**
     * @return
     */
    public Boolean IsVisible() {
        return isVisible.get();
    }

    /**
     * @return
     */
    public SimpleObjectProperty<Boolean> isVisibleProperty() {
        return isVisible;
    }

    /**
     * @param isVisible
     */
    public void setVisibility(Boolean isVisible) {
        this.isVisible.set(isVisible);
    }

    /**
     * @return
     */
    public List<RoadSegment3D> getRoads() {
        return roads.get();
    }

    /**
     * @param roads
     */
    public void setRoads(List<RoadSegment3D> roads) {
        this.roads.set(roads);
    }

    /**
     * @return
     */
    public SimpleObjectProperty<List<RoadSegment3D>> roadsProperty() {
        return roads;
    }

    /**
     * @return
     */
    public List<Cross3D> getCrosses() {
        return crosses.get();
    }

    /**
     * @param crosses
     */
    public void setCrosses(List<Cross3D> crosses) {
        this.crosses.set(crosses);
    }

    /**
     * @return
     */
    public SimpleObjectProperty<List<Cross3D>> crossesProperty() {
        return crosses;
    }

    /**
     * @param g
     */
    public void draw(Graphics2D g) {


        g.setStroke(new BasicStroke(10));
        g.setColor(Color.RED);
        x.drawTo2D(g);
        g.setColor(Color.GREEN);
        y.drawTo2D(g);
        g.setColor(Color.BLUE);
        z.drawTo2D(g);
        g.setColor(Color.BLACK);

        g.setStroke(new BasicStroke(1));
        for (RoadSegment3D tmp : getRoads())
            tmp.drawTo2D(g);

        for (Cross3D tmp : getCrosses())
            tmp.drawTo2D(g);

    }

    /**
     * @return
     */
    public ArrayList<Point2D> getAllPoints() {


        return point2DS;

    }

    /**
     * @return
     */
    public ArrayList<Shape3D> getShapes3D() {
        ArrayList<Shape3D> tmp = new ArrayList<>();

        tmp.addAll(getCrosses());
        tmp.addAll(getRoads());
        tmp.add(x);
        tmp.add(z);
        tmp.add(y);
        return tmp;


    }


}
