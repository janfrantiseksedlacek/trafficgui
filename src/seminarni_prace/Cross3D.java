package seminarni_prace;

import TrafficSim.*;
import javafx.beans.property.SimpleObjectProperty;

import java.awt.*;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 */
public class Cross3D extends Shape3D {

    /**
     *
     */
    private SimpleObjectProperty<List<Lane3D>> Lanes = new SimpleObjectProperty<>();

    /**
     *
     */
    private SimpleObjectProperty<Polygon> HitBox = new SimpleObjectProperty<>(new Polygon());

    /**
     *
     */
    private SimpleObjectProperty<CrossRoad> ParentCross = new SimpleObjectProperty<>();

    /**
     *
     */
    private SimpleObjectProperty<ArrayList<TrafficLight2D>> lights = new SimpleObjectProperty<>();

    /**
     * @return
     */
    public ArrayList<TrafficLight2D> getLights() {
        return lights.get();
    }

    /**
     * @param lights
     */
    public void setLights(ArrayList<TrafficLight2D> lights) {
        this.lights.set(lights);
    }

    /**
     * @return
     */
    public SimpleObjectProperty<ArrayList<TrafficLight2D>> lightsProperty() {
        return lights;
    }


    /**
     * @param cross
     */
    public Cross3D(CrossRoad cross) {

        setLanes(new ArrayList<>());

        setLights(new ArrayList<>());

        setParentCross(cross);

        calculateSegments(cross);


    }

    /**
     * @param parent
     */
    public void calculateSegments(Object parent) {

        CrossRoad cross = (CrossRoad) parent;

        RoadSegment[] roads = cross.getRoads();
        EndPoint[] ends = cross.getEndPoints();

        for (Lane tmp : cross.getLanes()) {

            //===================================================
            //Vstupni silnice a její vlastnosti + Sin a Cos
            //===================================================
            Point2D start = tmp.getStartRoad().getStartPosition();
            Point2D end = tmp.getStartRoad().getEndPosition();
            double sep = tmp.getStartRoad().getLaneSeparatorWidth();
            double lineSize = tmp.getStartRoad().getLaneWidth();
            //=========
            double ang = getAngle(start, end);
            double sin = Math.sin(Math.toRadians(ang)) * -1;
            double cos = Math.cos(Math.toRadians(ang)) * -1;
            //===================================================

            //===================================================
            //Vytupni silnice a její vlastnosti + Sin a Cos
            //===================================================
            Point2D start2 = tmp.getEndRoad().getStartPosition();
            Point2D end2 = tmp.getEndRoad().getEndPosition();
            double sep2 = tmp.getEndRoad().getLaneSeparatorWidth();
            double lineSize2 = tmp.getEndRoad().getLaneWidth();
            //=========
            double ang2 = getAngle(start2, end2);
            double sin2 = Math.sin(Math.toRadians(ang2)) * -1;
            double cos2 = Math.cos(Math.toRadians(ang2)) * -1;
            //===================================================

            //===================================================================================================
            // Vypocet bodu spoju v krizovatce
            //
            // stejna logika jako v silnicich
            //===================================================================================================


            if (ends[Arrays.asList(roads).indexOf(tmp.getStartRoad())] == EndPoint.START) {
                if (tmp.getStartLaneNumber() >= 0) {

                    start.setLocation(start.getX() - lineSize * sin * tmp.getStartLaneNumber(), start.getY() - lineSize * cos * tmp.getStartLaneNumber());
                } else {
                    start.setLocation(start.getX() - sep * sin, start.getY() + sep * cos);

                    start.setLocation(start.getX() + lineSize * sin * tmp.getStartLaneNumber(), start.getY() - lineSize * cos * tmp.getStartLaneNumber());


                }


            } else {
                if (tmp.getStartLaneNumber() >= 0) {
                    start.setLocation(end.getX(), end.getY());

                    start.setLocation(end.getX() + lineSize * sin * (tmp.getStartLaneNumber() - 1), end.getY() - lineSize * cos * (tmp.getStartLaneNumber() - 1));

                } else {
                    start.setLocation(end.getX() - sep * sin, end.getY() + sep * cos);

                    start.setLocation(end.getX() + lineSize * sin * tmp.getStartLaneNumber(), end.getY() - lineSize * cos * tmp.getStartLaneNumber());


                }


            }

            if (ends[Arrays.asList(roads).indexOf(tmp.getEndRoad())] == EndPoint.START) {
                if (tmp.getEndLaneNumber() >= 0) {

                    end2.setLocation(start2.getX() + lineSize2 * sin2 * (tmp.getEndLaneNumber() - 1), start2.getY() - lineSize2 * cos2 * (tmp.getEndLaneNumber() - 1));

                } else {
                    end2.setLocation(start2.getX() - sep2 * sin2, start2.getY() + sep2 * cos2);

                    end2.setLocation(start2.getX() + lineSize2 * sin2 * tmp.getEndLaneNumber(), start2.getY() - lineSize2 * cos2 * tmp.getEndLaneNumber());


                }

            } else {

                if (tmp.getEndLaneNumber() >= 0) {
                    end2.setLocation(end2.getX(), end2.getY());

                    end2.setLocation(end2.getX() + lineSize2 * sin2 * (tmp.getEndLaneNumber() - 1), end2.getY() - lineSize2 * cos2 * (tmp.getEndLaneNumber() - 1));

                } else {
                    end2.setLocation(end2.getX() - sep2 * sin2, end2.getY() + sep2 * cos2);

                    end2.setLocation(end2.getX() + lineSize2 * sin2 * tmp.getEndLaneNumber(), end2.getY() - lineSize2 * cos2 * tmp.getEndLaneNumber());


                }


            }

            //===================================================================================================


            getLanes().add(new Lane3D(tmp,new Line3D(new Point3D(start.getX(),start.getY(),0), new Point3D(end2.getX(), end2.getY(),0),0)));

            HitBox.getValue().addPoint((int) start.getX(), (int) start.getY());
            HitBox.getValue().addPoint((int) end2.getX(), (int) end2.getY());



         //=================================================================================================================================



        }



        TrafficLight[] lights = getParentCross().getTrafficLights(Direction.Entry);

        for(int i=1 ; i<lights.length; i++) {

            if(lights[i]==null) continue;

            Point2D tmp2D=new Point2D.Double(0,0);

            if (ends[Arrays.asList(roads).indexOf(roads[Direction.Entry.ordinal()])] == EndPoint.START)
                tmp2D.setLocation(roads[Direction.Entry.ordinal()].getStartPosition());
            else
                tmp2D.setLocation(roads[Direction.Entry.ordinal()].getEndPosition());

            tmp2D.setLocation(tmp2D.getX()+(i-1)*4, tmp2D.getY());

            getLights().add(new TrafficLight2D(lights[i], tmp2D));


        }



        lights = getParentCross().getTrafficLights(Direction.Left);

        for(int i=1 ; i<lights.length; i++) {

            if(lights[i]==null) continue;

            Point2D tmp2D=new Point2D.Double(0,0);

            if (ends[Arrays.asList(roads).indexOf(roads[Direction.Left.ordinal()])] == EndPoint.START)
                tmp2D.setLocation(roads[Direction.Left.ordinal()].getStartPosition());
            else
                tmp2D.setLocation(roads[Direction.Left.ordinal()].getEndPosition());

            tmp2D.setLocation(tmp2D.getX()+(i-1)*4, tmp2D.getY());

            getLights().add(new TrafficLight2D(lights[i], tmp2D));


        }



        lights = getParentCross().getTrafficLights(Direction.Right);

        for(int i=1 ; i<lights.length; i++) {

            if(lights[i]==null) continue;

            Point2D tmp2D=new Point2D.Double(0,0);

            if (ends[Arrays.asList(roads).indexOf(roads[Direction.Right.ordinal()])] == EndPoint.START)
                tmp2D.setLocation(roads[Direction.Right.ordinal()].getStartPosition());
            else
                tmp2D.setLocation(roads[Direction.Right.ordinal()].getEndPosition());


            tmp2D.setLocation(tmp2D.getX()+(i-1)*4, tmp2D.getY());

            getLights().add(new TrafficLight2D(lights[i], tmp2D));


        }


        lights = getParentCross().getTrafficLights(Direction.Opposite);

        for(int i=1 ; i<lights.length; i++) {

            if(lights[i]==null) continue;

            Point2D tmp2D=new Point2D.Double(0,0);

            if (ends[Arrays.asList(roads).indexOf(roads[Direction.Opposite.ordinal()])] == EndPoint.START)
                tmp2D.setLocation(roads[Direction.Opposite.ordinal()].getStartPosition());
            else
                tmp2D.setLocation(roads[Direction.Opposite.ordinal()].getEndPosition());


            tmp2D.setLocation(tmp2D.getX()+(i-1)*4, tmp2D.getY());

            getLights().add(new TrafficLight2D(lights[i], tmp2D));


        }



    }

    /**
     * @param start
     * @param end
     * @return
     */
    public double getAngle(Point2D start, Point2D end) {
        double angle = (float) Math.toDegrees(Math.atan2(start.getY() - end.getY(), start.getX() - end.getX()));

        if (angle < 0) {
            angle += 360;
        }

        return angle;
    }


    /**
     * @return
     */
    public List<Lane3D> getLanes() {
        return Lanes.get();
    }

    /**
     * @param lanes
     */
    public void setLanes(List<Lane3D> lanes) {
        this.Lanes.set(lanes);
    }

    /**
     * @return
     */
    public SimpleObjectProperty<List<Lane3D>> lanesProperty() {
        return Lanes;
    }

    /**
     * @return
     */
    public Polygon getHitBox() {
        return HitBox.get();
    }

    /**
     * @param hitBox
     */
    public void setHitBox(Polygon hitBox) {
        this.HitBox.set(hitBox);
    }

    /**
     * @return
     */
    public SimpleObjectProperty<Polygon> hitBoxProperty() {
        return HitBox;
    }

    /**
     * @return
     */
    public CrossRoad getParentCross() {
        return ParentCross.get();
    }

    /**
     * @param parentCross
     */
    public void setParentCross(CrossRoad parentCross) {
        this.ParentCross.set(parentCross);
    }

    /**
     * @return
     */
    public SimpleObjectProperty<CrossRoad> parentCrossProperty() {
        return ParentCross;
    }

    /**
     * @return
     */
    @Override
    public String toString() {

      return "Cross: " + ParentCross.getValue().getId();
    }

    /**
     * @param g
     */
    @Override
    public void drawTo2D(Graphics2D g) {
        for (Lane3D tmp:getLanes())
            tmp.getLine().drawTo2D(g);

    }

    /**
     * @param p
     */
    @Override
    public void setPosition(Point3D p) {
        for (Lane3D tmp:getLanes())
            tmp.getLine().setPosition(p);

    }

    /**
     * @param alfa
     */
    @Override
    public void transformAroundZ(double alfa) {
        for (Lane3D tmp:getLanes())
            tmp.getLine().transformAroundZ(alfa);

    }

    /**
     * @param alfa
     */
    @Override
    public void transformAroundX(double alfa) {
        for (Lane3D tmp:getLanes())
            tmp.getLine().transformAroundX(alfa);
    }

    /**
     * @param alfa
     */
    @Override
    public void transformAroundY(double alfa) {
        for (Lane3D tmp:getLanes())
            tmp.getLine().transformAroundY(alfa);
    }
}
