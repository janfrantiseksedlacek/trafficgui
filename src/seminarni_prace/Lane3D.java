package seminarni_prace;

import TrafficSim.Lane;

import java.awt.geom.Line2D;


/**
 *
 */
public class Lane3D {

    /**
     *
     */
    private Lane ParentLane;

    /**
     *
     */
    private Line3D Line;

    /**
     * @param parentLane
     * @param line
     */
    public Lane3D(Lane parentLane, Line3D line) {
        ParentLane = parentLane;
        Line = line;
    }

    /**
     * @param parentLane
     * @param line
     */
    public Lane3D(Lane parentLane, Line2D line) {
        ParentLane = parentLane;
        Line = new Line3D(new Point3D(line.getX1(), line.getY1(),0), new Point3D(line.getX2(), line.getY2(), 0), 0);
    }

    /**
     * @return
     */
    public Lane getParentLane() {
        return ParentLane;
    }

    /**
     * @param parentLane
     */
    public void setParentLane(Lane parentLane) {
        ParentLane = parentLane;
    }

    /**
     * @return
     */
    public Line3D getLine() {
        return Line;
    }

    /**
     * @param line
     */
    public void setLine(Line3D line) {
        Line = line;
    }
}
