package seminarni_prace;

import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;

/**
 *
 */
public interface IClickedOnObject {

    /**
     * @param object2D
     * @throws NoninvertibleTransformException
     */
    void clickedObject(Object2D object2D) throws NoninvertibleTransformException;
}
