package seminarni_prace;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 *
 */
public class RelaxingCube3Dsimulator extends JPanel implements ActionListener {

    /**
     *
     */
    private ArrayList<Shape3D> shape3DS = new ArrayList<>();

    /**
     *
     */
    private javax.swing.Timer timer = new Timer(1, this::actionPerformed);

    /**
     *
     */
    public RelaxingCube3Dsimulator() {

        shape3DS.add(new Cube3D(0, 0, 0, 100, 100, 100, 100));
        shape3DS.add(new Cube3D(0, 0, 0, 100, 100, -100, 100));
        shape3DS.add(new Cube3D(0, 0, 0, 100, -100, 100, 100));
        shape3DS.add(new Cube3D(0, 0, 0, -100, -100, 100, 100));

        timer.start();

    }

    /**
     * @param g
     */
    @Override
    public void paintComponent(Graphics g) {


        Graphics2D g3 = (Graphics2D) g;


        g3.translate(0, getHeight());


        g3.scale(1, -1);

        super.paintComponent(g);

        g.translate(getWidth() / 2, getHeight() / 2);

        g.setColor(Color.GREEN);

        for (Shape3D tmp : shape3DS)
            tmp.drawTo2D((Graphics2D) g);


    }

    /**
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        for (Shape3D tmp : shape3DS) {
            tmp.transformAroundZ(1);
            tmp.transformAroundX(0.5);
            tmp.transformAroundY(1);
        }
        repaint();
    }
}
