package seminarni_prace;

import java.awt.*;
import java.awt.geom.Point2D;

/**
 *
 */
public abstract class Object2D implements IDraw2D {

    /**
     * @return
     */
    public Color getColor() {
        return color;
    }

    /**
     * @param color
     */
    public void setColor(Color color) {
        this.color = color;
    }

    /**
     *
     */
    private Color color =Color.BLACK;

    /**
     * @return
     */
    public Boolean getSelected() {/**/
        return Selected;
    }

    /**
     * @param selected
     */
    public void setSelected(Boolean selected) {
        Selected = selected;
    }

    /**
     *
     */
    private Boolean Selected =false;

    /**
     * @param start
     * @param end
     * @return
     */
    @Override
    public double getAngle(Point2D start, Point2D end) {
        double angle = (float) Math.toDegrees(Math.atan2(start.getY() - end.getY(), start.getX() - end.getX()));

        if (angle < 0) {
            angle += 360;
        }

        return angle;
    }

    /**
     * @return
     */
    @Override
    public String toString() {
        return "Object2D";
    }





}
