package seminarni_prace;

import TrafficSim.*;
import javafx.beans.property.SimpleObjectProperty;

import java.awt.*;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 */
public class Cross2D extends Object2D {

    /**
     *Vsechny lajny
     */
    private SimpleObjectProperty<List<Line2D>> Lanes = new SimpleObjectProperty<>();

    /**
     *Hitboxy objektu
     */
    private SimpleObjectProperty<Polygon> HitBox = new SimpleObjectProperty<>(new Polygon());

    /**
     * dedena krizovatka
     */
    private SimpleObjectProperty<CrossRoad> ParentCross = new SimpleObjectProperty<>();

    /**
     * Vsechny semafory
     */
    private SimpleObjectProperty<ArrayList<TrafficLight2D>> lights = new SimpleObjectProperty<>();

    /**
     * @return
     */
    public ArrayList<TrafficLight2D> getLights() {
        return lights.get();
    }

    /**
     * @param lights
     */
    public void setLights(ArrayList<TrafficLight2D> lights) {
        this.lights.set(lights);
    }

    /**
     * @return
     */
    public SimpleObjectProperty<ArrayList<TrafficLight2D>> lightsProperty() {
        return lights;
    }


    /**
     * @param cross
     */
    public Cross2D(CrossRoad cross) {

        setLanes(new ArrayList<>());

        setLights(new ArrayList<>());

        setParentCross(cross);

        calculateSegments(cross);


    }

    /**
     * @param g
     */
    @Override
    public void draw(Graphics2D g) {
        if (getSelected())
            g.setColor(Color.BLUE);
        else
            g.setColor(Color.BLACK);

        g.setStroke(new BasicStroke(2));

        {
            for (Line2D tmp : getLanes())
                g.draw(tmp);


        }


        for (TrafficLight2D tmp : getLights())
        {
        if(tmp.isVisible())
            tmp.draw(g);
        }


    }

    /**
     * @param parent
     */
    @Override
    public void calculateSegments(Object parent) {

        CrossRoad cross = (CrossRoad) parent;

        RoadSegment[] roads = cross.getRoads();
        EndPoint[] ends = cross.getEndPoints();

        for (Lane tmp : cross.getLanes()) {

            //===================================================
            //Vstupni silnice a její vlastnosti + Sin a Cos
            //===================================================
            Point2D start = tmp.getStartRoad().getStartPosition();
            Point2D end = tmp.getStartRoad().getEndPosition();
            double sep = tmp.getStartRoad().getLaneSeparatorWidth();
            double lineSize = tmp.getStartRoad().getLaneWidth();
            //=========
            double ang = getAngle(start, end);
            double sin = Math.sin(Math.toRadians(ang)) * -1;
            double cos = Math.cos(Math.toRadians(ang)) * -1;
            //===================================================

            //===================================================
            //Vytupni silnice a její vlastnosti + Sin a Cos
            //===================================================
            Point2D start2 = tmp.getEndRoad().getStartPosition();
            Point2D end2 = tmp.getEndRoad().getEndPosition();
            double sep2 = tmp.getEndRoad().getLaneSeparatorWidth();
            double lineSize2 = tmp.getEndRoad().getLaneWidth();
            //=========
            double ang2 = getAngle(start2, end2);
            double sin2 = Math.sin(Math.toRadians(ang2)) * -1;
            double cos2 = Math.cos(Math.toRadians(ang2)) * -1;
            //===================================================

            //===================================================================================================
            // Vypocet bodu spoju v krizovatce
            //
            // stejna logika jako v silnicich
            //===================================================================================================


            if (ends[Arrays.asList(roads).indexOf(tmp.getStartRoad())] == EndPoint.START) {
                if (tmp.getStartLaneNumber() >= 0) {

                    start.setLocation(start.getX() - lineSize * sin * tmp.getStartLaneNumber(), start.getY() - lineSize * cos * tmp.getStartLaneNumber());
                } else {
                    start.setLocation(start.getX() - sep * sin, start.getY() + sep * cos);

                    start.setLocation(start.getX() + lineSize * sin * tmp.getStartLaneNumber(), start.getY() - lineSize * cos * tmp.getStartLaneNumber());


                }


            } else {
                if (tmp.getStartLaneNumber() >= 0) {
                    start.setLocation(end.getX(), end.getY());

                    start.setLocation(end.getX() + lineSize * sin * (tmp.getStartLaneNumber() - 1), end.getY() - lineSize * cos * (tmp.getStartLaneNumber() - 1));

                } else {
                    start.setLocation(end.getX() - sep * sin, end.getY() + sep * cos);

                    start.setLocation(end.getX() + lineSize * sin * tmp.getStartLaneNumber(), end.getY() - lineSize * cos * tmp.getStartLaneNumber());


                }


            }

            if (ends[Arrays.asList(roads).indexOf(tmp.getEndRoad())] == EndPoint.START) {
                if (tmp.getEndLaneNumber() >= 0) {

                    end2.setLocation(start2.getX() + lineSize2 * sin2 * (tmp.getEndLaneNumber() - 1), start2.getY() - lineSize2 * cos2 * (tmp.getEndLaneNumber() - 1));

                } else {
                    end2.setLocation(start2.getX() - sep2 * sin2, start2.getY() + sep2 * cos2);

                    end2.setLocation(start2.getX() + lineSize2 * sin2 * tmp.getEndLaneNumber(), start2.getY() - lineSize2 * cos2 * tmp.getEndLaneNumber());


                }

            } else {

                if (tmp.getEndLaneNumber() >= 0) {
                    end2.setLocation(end2.getX(), end2.getY());

                    end2.setLocation(end2.getX() + lineSize2 * sin2 * (tmp.getEndLaneNumber() - 1), end2.getY() - lineSize2 * cos2 * (tmp.getEndLaneNumber() - 1));

                } else {
                    end2.setLocation(end2.getX() - sep2 * sin2, end2.getY() + sep2 * cos2);

                    end2.setLocation(end2.getX() + lineSize2 * sin2 * tmp.getEndLaneNumber(), end2.getY() - lineSize2 * cos2 * tmp.getEndLaneNumber());


                }


            }

            //===================================================================================================


            getLanes().add(new Line2D.Double(start, end2));

            HitBox.getValue().addPoint((int) start.getX(), (int) start.getY());
            HitBox.getValue().addPoint((int) end2.getX(), (int) end2.getY());



         //=================================================================================================================================



        }



        TrafficLight[] lights = getParentCross().getTrafficLights(Direction.Entry);

        for(int i=1 ; i<lights.length; i++) {

            if(lights[i]==null) continue;

            Point2D tmp2D=new Point2D.Double(0,0);

            if (ends[Arrays.asList(roads).indexOf(roads[Direction.Entry.ordinal()])] == EndPoint.START)
                tmp2D.setLocation(roads[Direction.Entry.ordinal()].getStartPosition());
            else
                tmp2D.setLocation(roads[Direction.Entry.ordinal()].getEndPosition());

            tmp2D.setLocation(tmp2D.getX()+(i-1)*4, tmp2D.getY());

            getLights().add(new TrafficLight2D(lights[i], tmp2D));


        }



        lights = getParentCross().getTrafficLights(Direction.Left);

        for(int i=1 ; i<lights.length; i++) {

            if(lights[i]==null) continue;

            Point2D tmp2D=new Point2D.Double(0,0);

            if (ends[Arrays.asList(roads).indexOf(roads[Direction.Left.ordinal()])] == EndPoint.START)
                tmp2D.setLocation(roads[Direction.Left.ordinal()].getStartPosition());
            else
                tmp2D.setLocation(roads[Direction.Left.ordinal()].getEndPosition());

            tmp2D.setLocation(tmp2D.getX()+(i-1)*4, tmp2D.getY());

            getLights().add(new TrafficLight2D(lights[i], tmp2D));


        }



        lights = getParentCross().getTrafficLights(Direction.Right);

        for(int i=1 ; i<lights.length; i++) {

            if(lights[i]==null) continue;

            Point2D tmp2D=new Point2D.Double(0,0);

            if (ends[Arrays.asList(roads).indexOf(roads[Direction.Right.ordinal()])] == EndPoint.START)
                tmp2D.setLocation(roads[Direction.Right.ordinal()].getStartPosition());
            else
                tmp2D.setLocation(roads[Direction.Right.ordinal()].getEndPosition());


            tmp2D.setLocation(tmp2D.getX()+(i-1)*4, tmp2D.getY());

            getLights().add(new TrafficLight2D(lights[i], tmp2D));


        }


        lights = getParentCross().getTrafficLights(Direction.Opposite);

        for(int i=1 ; i<lights.length; i++) {

            if(lights[i]==null) continue;

            Point2D tmp2D=new Point2D.Double(0,0);

            if (ends[Arrays.asList(roads).indexOf(roads[Direction.Opposite.ordinal()])] == EndPoint.START)
                tmp2D.setLocation(roads[Direction.Opposite.ordinal()].getStartPosition());
            else
                tmp2D.setLocation(roads[Direction.Opposite.ordinal()].getEndPosition());


            tmp2D.setLocation(tmp2D.getX()+(i-1)*4, tmp2D.getY());

            getLights().add(new TrafficLight2D(lights[i], tmp2D));


        }



    }


    /**
     * @return
     */
    public List<Line2D> getLanes() {
        return Lanes.get();
    }

    /**
     * @param lanes
     */
    public void setLanes(List<Line2D> lanes) {
        this.Lanes.set(lanes);
    }

    /**
     * @return
     */
    public SimpleObjectProperty<List<Line2D>> lanesProperty() {
        return Lanes;
    }

    /**
     * @return
     */
    public Polygon getHitBox() {
        return HitBox.get();
    }

    /**
     * @param hitBox
     */
    public void setHitBox(Polygon hitBox) {
        this.HitBox.set(hitBox);
    }

    /**
     * @return
     */
    public SimpleObjectProperty<Polygon> hitBoxProperty() {
        return HitBox;
    }

    /**
     * @return
     */
    public CrossRoad getParentCross() {
        return ParentCross.get();
    }

    /**
     * @param parentCross
     */
    public void setParentCross(CrossRoad parentCross) {
        this.ParentCross.set(parentCross);
    }

    /**
     * @return
     */
    public SimpleObjectProperty<CrossRoad> parentCrossProperty() {
        return ParentCross;
    }

    /**
     * @return
     */
    @Override
    public String toString() {

      return "Cross: " + ParentCross.getValue().getId();
    }
}
