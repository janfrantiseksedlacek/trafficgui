package seminarni_prace;

import TrafficSim.RoadSegment;
import javafx.beans.property.SimpleObjectProperty;

import java.awt.*;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class RoadSegment3D extends Shape3D {

    /**
     *
     */
    private SimpleObjectProperty<List<Lane3D>>Lanes=new SimpleObjectProperty<>();

    /**
     *
     */
    private SimpleObjectProperty<Polygon>HitBox= new SimpleObjectProperty<>(new Polygon());

    /**
     *
     */
    private SimpleObjectProperty<RoadSegment> ParentRoad=new SimpleObjectProperty<>();

    /**
     * @return
     */
    public Color getColor() {
        return color;
    }

    /**
     * @param color
     */
    public void setColor(Color color) {
        this.color = color;
    }

    /**
     *
     */
    private Color color=Color.BLACK;

    /**
     * @param road
     */
    public RoadSegment3D(RoadSegment road) {


        setLanes(new ArrayList<>());

        setParentRoad(road);

        calculateSegments(road);


    }

    /**
     * @return
     */
    public boolean isSelected() {
        return isSelected;
    }

    /**
     * @param selected
     */
    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    /**
     *
     */
    private boolean isSelected=false;

    /**
     * @return
     */
    public List<Lane3D> getLanes() {
        return Lanes.get();
    }

    /**
     * @return
     */
    public SimpleObjectProperty<List<Lane3D>> lanesProperty() {
        return Lanes;
    }

    /**
     * @param lanes
     */
    public void setLanes(List<Lane3D> lanes) {
        this.Lanes.set(lanes);
    }

    /**
     * @return
     */
    public Polygon getHitBox() {
        return HitBox.get();
    }

    /**
     * @return
     */
    public SimpleObjectProperty<Polygon> hitBoxProperty() {
        return HitBox;
    }

    /**
     * @param hitBox
     */
    public void setHitBox(Polygon hitBox) {
        this.HitBox.set(hitBox);
    }

    /**
     * @return
     */
    public RoadSegment getParentRoad() {
        return ParentRoad.get();
    }

    /**
     * @return
     */
    public SimpleObjectProperty<RoadSegment> parentRoadProperty() {
        return ParentRoad;
    }

    /**
     * @param parentRoad
     */
    public void setParentRoad(RoadSegment parentRoad) {
        this.ParentRoad.set(parentRoad);
    }


    /**
     * @param start
     * @param end
     * @return
     */
    public double getAngle(Point2D start, Point2D end) {
        double angle = (float) Math.toDegrees(Math.atan2(start.getY() - end.getY(), start.getX() - end.getX()));

        if (angle < 0) {
            angle += 360;
        }

        return angle;
    }


    /**
     * @param parent
     */
    public void calculateSegments(Object parent) {

        RoadSegment road=(RoadSegment)parent;

        //=====================
        //Promnene
        //=====================
        Point2D start = road.getStartPosition();
        Point2D end = road.getEndPosition();
        double forward = road.getForwardLanesCount();
        double backward = road.getBackwardLanesCount();
        double sep = road.getLaneSeparatorWidth();
        double lineSize = road.getLaneWidth();
        //=====================

        //=====================
        // Sin a Cos pro posuny
        //=====================
        double ang = getAngle(start, end);
        double sin = Math.sin(Math.toRadians(ang)) * -1;
        double cos = Math.cos(Math.toRadians(ang)) * -1;
        //=====================



        //========================================
        //  Rovnice
        //
        //  X[i]= X[i-1] +- sirkaSilnice * sin
        //  Y[i]= Y[i-1] +- sirkaSilnice * cos
        //========================================

        for (int i = 0; i < forward; i++) {



            getLanes().add(new Lane3D(road.getLane(i+1),new Line2D.Double(start,end)));
            HitBox.getValue().addPoint((int)start.getX(), (int)start.getY());
            HitBox.getValue().addPoint((int)end.getX(), (int)end.getY());

            start.setLocation(start.getX() + lineSize * sin, start.getY() - lineSize * cos);

            end.setLocation(end.getX() + lineSize * sin, end.getY() - lineSize * cos);

        }

        //=============================================
        //Posunuti "kreslitka" na druhou stranu silnice
        //=============================================

        start = road.getStartPosition();
        end = road.getEndPosition();

        start.setLocation(start.getX() - sep * sin, start.getY() + sep * cos);

        end.setLocation(end.getX() - sep * sin, end.getY() + sep * cos);

        start.setLocation(start.getX() - lineSize * sin, start.getY() + lineSize * cos);

        end.setLocation(end.getX() - lineSize * sin, end.getY() + lineSize * cos);

        //=============================================

        for (double i = backward; i > 0; i--) {

            getLanes().add(new Lane3D(road.getLane(-(int)(backward-i)-1),new Line2D.Double(start,end)));
            HitBox.getValue().addPoint((int)start.getX(), (int)start.getY());
            HitBox.getValue().addPoint((int)end.getX(), (int)end.getY());

            start.setLocation(start.getX() - lineSize * sin, start.getY() + lineSize * cos);

            end.setLocation(end.getX() - lineSize * sin, end.getY() + lineSize * cos);

        }





    }


    /**
     * @return
     */
    @Override
    public String toString() {
        return "" +
                "Name: " + ParentRoad.getValue().getId() +
                ", Lanes: " + Lanes.getValue().size();
    }

    /**
     * @param g
     */
    @Override
    public void drawTo2D(Graphics2D g)
    {

        for (Lane3D tmp:getLanes())
            tmp.getLine().drawTo2D(g);

    }

    /**
     * @param p
     */
    @Override
    public void setPosition(Point3D p) {
        for (Lane3D tmp:getLanes())
            tmp.getLine().setPosition(p);
    }

    /**
     * @param alfa
     */
    @Override
    public void transformAroundZ(double alfa) {
        for (Lane3D tmp:getLanes())
            tmp.getLine().transformAroundZ(alfa);
    }

    /**
     * @param alfa
     */
    @Override
    public void transformAroundX(double alfa) {
        for (Lane3D tmp:getLanes())
            tmp.getLine().transformAroundX(alfa);
    }

    /**
     * @param alfa
     */
    @Override
    public void transformAroundY(double alfa) {
        for (Lane3D tmp:getLanes())
            tmp.getLine().transformAroundY(alfa);
    }
}