package seminarni_prace;

import TrafficSim.CrossRoad;
import TrafficSim.RoadSegment;
import javafx.beans.property.SimpleObjectProperty;

import java.awt.*;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class TrafficState  {

    /**
     *
     */
    private SimpleObjectProperty<List<RoadSegment2D>> roads = new SimpleObjectProperty<>(new ArrayList<>());

    /**
     *
     */
    private SimpleObjectProperty<List<Cross2D>> crosses = new SimpleObjectProperty<>(new ArrayList<>());

    /**
     *
     */
    private ArrayList<Point2D> point2DS = new ArrayList<>();

    /**
     * @return
     */
    public Boolean IsVisible() {
        return isVisible.get();
    }

    /**
     * @return
     */
    public SimpleObjectProperty<Boolean> isVisibleProperty() {
        return isVisible;
    }

    /**
     * @param isVisible
     */
    public void setVisibility(Boolean isVisible) {
        this.isVisible.set(isVisible);
    }

    /**
     *
     */
    private SimpleObjectProperty<Boolean> isVisible = new SimpleObjectProperty<>(true);

    /**
     * @param roads
     * @param crosses
     */
    public TrafficState(RoadSegment[] roads, CrossRoad[] crosses)
    {
        ArrayList<Cross2D> tmpC=new ArrayList<>();

        ArrayList<RoadSegment2D> tmpR=new ArrayList<>();

        for (CrossRoad c:crosses)
        {
        tmpC.add(new Cross2D(c));

        }

        for (RoadSegment c:roads)
        {
            tmpR.add(new RoadSegment2D(c));

        }

        setCrosses(tmpC);

        setRoads(tmpR);

        for (Cross2D tmp:getCrosses())
        {
            for (Line2D tmp1:tmp.getLanes())
            {

                point2DS.add(tmp1.getP1());
                point2DS.add(tmp1.getP2());

            }
        }

        for (RoadSegment2D tmp:getRoads())
        {
            for (Lane2D tmp1:tmp.getLanes())
            {

                point2DS.add(tmp1.getLine().getP1());
                point2DS.add(tmp1.getLine().getP2());

            }
        }


    }

    /**
     * @return
     */
    public List<RoadSegment2D> getRoads() {
        return roads.get();
    }

    /**
     * @return
     */
    public SimpleObjectProperty<List<RoadSegment2D>> roadsProperty() {
        return roads;
    }

    /**
     * @param roads
     */
    public void setRoads(List<RoadSegment2D> roads) {
        this.roads.set(roads);
    }

    /**
     * @return
     */
    public List<Cross2D> getCrosses() {
        return crosses.get();
    }

    /**
     * @return
     */
    public SimpleObjectProperty<List<Cross2D>> crossesProperty() {
        return crosses;
    }

    /**
     * @param crosses
     */
    public void setCrosses(List<Cross2D> crosses) {
        this.crosses.set(crosses);
    }

    /**
     *
     */
    public void unSelectedAll()
    {
        for (Cross2D tmp:getCrosses())
            tmp.setSelected(false);

        for (RoadSegment2D tmp:getRoads())
            tmp.setSelected(false);

    }


    /**
     * @param g
     */
    public void draw(Graphics2D g)
    {



        for (RoadSegment2D tmp:getRoads())
            tmp.draw(g);

        for (Cross2D tmp:getCrosses())
            tmp.draw(g);

    }

    /**
     *
     */
    public void setDefaultColor()
    {
        for (Cross2D tmp:getCrosses())
            tmp.setColor(Color.DARK_GRAY);

        for (RoadSegment2D tmp:getRoads())
            tmp.setColor(Color.DARK_GRAY);

    }


    /**
     * @return
     */
    public ArrayList<Point2D> getAllPoints()
    {


        return point2DS;

    }


}
