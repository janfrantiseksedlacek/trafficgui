package seminarni_prace;


import TrafficSim.Car;
import TrafficSim.Simulator;
import javafx.beans.property.SimpleObjectProperty;
import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;


/**
 *
 */
public class DrawingPanel3D extends JPanel  {

    /**
     *
     */
    TrafficState3D trafficState;

    /**
     * @return
     */
    public TrafficState3D getTrafficState() {
        return trafficState;
    }

    /**
     * "The serialization runtime associates with each serializable class a version number, called a serialVersionUID"
     * {@linkplain "https://stackoverflow.com/questions/285793/what-is-a-serialversionuid-and-why-should-i-use-it" }
     */
    private static final long serialVersionUID = 1L;


    /**
     * Barva aut
     */
    private Color CarColor =new Color(241, 241, 241);


    /**
     * @return
     */
    public SimpleObjectProperty<Simulator> simulatorProperty() {
        return simulator;
    }


    /**
     * Prehravana simulace
     */
    private SimpleObjectProperty<Simulator> simulator = new SimpleObjectProperty<>();



    /**
     * Konstruktor
     * @param scenar ID scenare
     */
    public DrawingPanel3D(int scenar) {

        super();

        simulator.setValue(new Simulator());

        String[] scenarios = simulator.getValue().getScenarios();

        simulator.getValue().runScenario(scenarios[scenar]);

        trafficState=new TrafficState3D(simulatorProperty().getValue().getRoadSegments(), simulatorProperty().getValue().getCrossroads());

        setSize(1000,1000);


    }


    /**
     * @param g
     */
    public  void computeModelDimensions(Graphics2D g) {

        double MinX=0;
        double MinY=0;
        double MaxX=0;
        double MaxY=0;

        for (Point2D tmp:getTrafficState().getAllPoints())
        {

            if (tmp.getX() > MaxX)
                MaxX = tmp.getX();

            if (tmp.getY() > MaxY)
                MaxY = tmp.getY();

            if (tmp.getX() < MinX)
                MinX = tmp.getX();

            if (tmp.getY() < MinY)
                MinY = tmp.getY();

        }


        double Width = ( Math.abs(MaxX - MinX))+1;

        double Height = ( Math.abs(MaxY - MinY))+1;

        double scaleX=this.getWidth()/Width;

        double scaleY=this.getHeight()/Height;


        g.translate(-MinX*scaleX, -MinY*scaleY);

        double scale= Math.min(scaleX, scaleY);

        g.scale(scale, scale);




    }


    /**
     * @param g
     * @return
     */
    public Graphics2D computeg2(Graphics g)
    {


        Graphics2D g2 = (Graphics2D) g;

        computeModelDimensions(g2);

        return g2;

    }



    /**
     * Vykresli compenenty
     * @param g graficky kontent
     */
    @Override
    public void paintComponent(Graphics g) {



        Graphics2D g3 = (Graphics2D) g;


        g3.translate(0, getHeight());


        g3.scale(1, -1);



        super.paintComponent(g);

        Graphics2D g2 = computeg2(g);


        RenderingHints rh = new RenderingHints(
                RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        g2.setRenderingHints(rh);





        getTrafficState().draw(g2);

        drawCars(getSimulator().getCars(), g2);





    }


    /**
     * @param i
     */
    public void change(int i)
    {
        simulator.setValue(new Simulator());

        String[] scenarios = simulator.getValue().getScenarios();

        simulatorProperty().getValue().clear();

        simulator.getValue().runScenario(scenarios[i]);

        trafficState=new TrafficState3D(simulatorProperty().getValue().getRoadSegments(), simulatorProperty().getValue().getCrossroads());




    }


    /**
     * Vykresli jedno auto
     * @param car auto
     * @param g graficky kontent
     */
    void drawCar(Car car, Graphics2D g) {

        Line2D line = new Line2D.Double(car.getPosition().getX(), car.getPosition().getY(),
                car.getPosition().getX() + car.getLength() / 4, car.getPosition().getY());

        AffineTransform at = AffineTransform.getRotateInstance(car.getOrientation(), line.getX1(), line.getY1());

        g.setStroke(new BasicStroke(1));
        g.setColor(CarColor);
        g.draw(at.createTransformedShape(line));

    }


    /**
     * Vykresli pole aut
     * @param cars auta
     * @param g graficky kontent
     */
    void drawCars(Car[] cars, Graphics2D g) {

        for (Car tmp : cars)
           drawCar(tmp, g);

    }


    /**
     * Simulator
     * @return Simulator
     */
    public Simulator getSimulator()
    {
        return simulator.get();
    }





}
